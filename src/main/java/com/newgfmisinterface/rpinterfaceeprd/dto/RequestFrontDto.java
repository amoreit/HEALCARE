package com.newgfmisinterface.rpinterfaceeprd.dto;

public class RequestFrontDto {
    private String fileName;
    private String uploadPath;
    private String outputPath;
    private String agencyCode;
    private String postingDate;  //ตัวอย่างรูปแบบที่ต้องการ 20200326
    private String docType;
    private String createdUser;
    private String ipAddress;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public void setOutputPath(String outputPath) {
        this.outputPath = outputPath;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    public String toString() {
        return "RequestFrontDto{" +
                "fileName='" + fileName + '\'' +
                ", uploadPath='" + uploadPath + '\'' +
                ", outputPath='" + outputPath + '\'' +
                ", agencyCode='" + agencyCode + '\'' +
                ", postingDate='" + postingDate + '\'' +
                ", docType='" + docType + '\'' +
                ", createdUser='" + createdUser + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                '}';
    }
}

