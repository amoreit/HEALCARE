package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "REQUEST")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestDto {
    @XmlElement(name = "PARK_POST")
    private String parkPost;
    @XmlElement(name = "FLAG")
    private int flag;
    @XmlElement(name = "FORMID")
    private int formID;
    @XmlElement(name = "AUTODOC")
    private Boolean autoDoc = false;
    @XmlElement(name = "HEADER")
    private ZFIHeader header;
    @XmlElementWrapper(name = "ITEMS")
    @XmlElement(name = "ITEM")
    private List<ZFILine> lines = new ArrayList<>();
    @XmlElement(name = "FIRSTHEADER")
    private ZFIFromDoc firstHeader;

    public RequestDto() {
    }

    public RequestDto(String parkPost, int flag, int formID, Boolean autoDoc, ZFIHeader header, List<ZFILine> lines, ZFIFromDoc firstHeader) {
        this.parkPost = parkPost;
        this.flag = flag;
        this.formID = formID;
        this.autoDoc = autoDoc;
        this.header = header;
        this.lines = lines;
        this.firstHeader = firstHeader;
    }

    public String getParkPost() {
        return parkPost;
    }

    public void setParkPost(String parkPost) {
        this.parkPost = parkPost;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFormID() {
        return formID;
    }

    public void setFormID(int formID) {
        this.formID = formID;
    }

    public Boolean getAutoDoc() {
        return autoDoc;
    }

    public void setAutoDoc(Boolean autoDoc) {
        this.autoDoc = autoDoc;
    }

    public ZFIHeader getHeader() {
        return header;
    }

    public void setHeader(ZFIHeader header) {
        this.header = header;
    }

    public List<ZFILine> getLines() {
        return lines;
    }

    public void setLines(List<ZFILine> lines) {
        this.lines = lines;
    }

    public ZFIFromDoc getFirstHeader() {
        return firstHeader;
    }

    public void setFirstHeader(ZFIFromDoc firstHeader) {
        this.firstHeader = firstHeader;
    }

    @Override
    public String toString() {
        return "RequestDto{" +
                "parkPost='" + parkPost + '\'' +
                ", flag=" + flag +
                ", formID=" + formID +
                ", autoDoc=" + autoDoc +
                ", header=" + header +
                ", lines=" + lines +
                ", firstHeader=" + firstHeader +
                '}';
    }
}
