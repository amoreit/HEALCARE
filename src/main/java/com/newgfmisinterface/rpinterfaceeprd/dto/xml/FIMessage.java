package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.newgfmisinterface.rpinterfaceeprd.adapter.Base64Adapter;

@XmlRootElement(
        name = "FI"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class FIMessage extends BaseMQServiceXML {
    @XmlElement(
            name = "DATATYPE"
    )
    private String dataType = null;
    @XmlJavaTypeAdapter(Base64Adapter.class)
    @XmlElement(
            name = "DATA"
    )
    private String data = null;

    public FIMessage() {
    }

    public void setId(String clientValue, FIMessage.TableLog tableLog, int logID) {
        this.setId(clientValue + "." + tableLog.getCode() + "." + logID);
    }

    public boolean isValidID() {
        return Pattern.matches("\\d+.[IDM]L.\\d+", this.getId());
    }

    public FIMessage.FIID getIDValue() {
        if (!this.isValidID()) {
            return null;
        } else {
            FIMessage.FIID fID = new FIMessage.FIID();
            List<String> elephantList = Arrays.asList(this.getId().split("\\."));
            fID.setClientValue((String)elephantList.get(0));
            fID.setItemOrAutoDoc((String)elephantList.get(1));
            fID.setLogID(Integer.parseInt((String)elephantList.get(2)));
            return fID;
        }
    }

    public String getDataType() {
        return this.dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public static enum DataType {
        CREATE("CRE"),
        REVERSE("REV"),
        UPDATE_PBK("UPB"),
        VIEW("VIEW"),
        POST("POST"),
        VOID("VOID");

        private final String code;

        private DataType(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

    public class FIID {
        private String clientValue = null;
        private String itemOrAutoDoc = null;
        private int logID = 0;

        public FIID() {
        }

        public String getClientValue() {
            return this.clientValue;
        }

        public void setClientValue(String clientValue) {
            this.clientValue = clientValue;
        }

        public String getItemOrAutoDoc() {
            return this.itemOrAutoDoc;
        }

        public void setItemOrAutoDoc(String itemOrAutoDoc) {
            this.itemOrAutoDoc = itemOrAutoDoc;
        }

        public int getLogID() {
            return this.logID;
        }

        public void setLogID(int logID) {
            this.logID = logID;
        }
    }

    public static enum QueueName {
        FI_AUTODOC("FI.AutoDoc"),
        RPINF_3TAX("RPInf.3TaxComp"),
        RPINF_RECEIVE_DOC("RPInf.ReceiveDoc"),
        RPINF_CAA("RPInf.Caa"),
        GL_RECEIVE_DOC("GL.ReceiveDoc"),
        GL_POST("GL.Post"),
        AP_RECEIVE_DOC("AP.ReceiveDoc"),
        AP_UP_PBK("AP.UpPbk"),
        AP_PAYMENT("AP.Payment");

        private final String code;

        private QueueName(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

    public static enum System {
        PAYMENT("PaySys"),
        GLPOST("GLPost"),
        RPINF("RPINF");

        private final String code;

        private System(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

    public static enum TableLog {
        HEADER_LOG("HL"),
        ITEM_LOG("IL"),
        AUTO_DOC_LOG("DL"),
        RECEIVE_AUTO_DOC_LOG("RA"),
        RECEIVE_MQ_LOG("MH"),
        RECEIVE_MQ_LINE_LOG("ML");

        private final String code;

        private TableLog(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

    public static enum Type {
        REQUEST("REQ"),
        RESPONSE("RES");

        private final String code;

        private Type(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

}

