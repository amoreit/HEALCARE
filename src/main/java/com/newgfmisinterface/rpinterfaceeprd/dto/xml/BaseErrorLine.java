package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class BaseErrorLine {
    @XmlElement(
            name = "TYPE"
    )
    private String type;
    @XmlElement(
            name = "CODE"
    )
    private String code;
    @XmlElement(
            name = "LINE"
    )
    private int line;
    @XmlElement(
            name = "TEXT"
    )
    private String text;

    public BaseErrorLine() {
        this.type = "E";
        this.code = null;
        this.line = 0;
        this.text = null;
    }

    public BaseErrorLine(String type, String code, String text) {
        this.type = "E";
        this.code = null;
        this.line = 0;
        this.text = null;
        this.type = type;
        this.code = code;
        this.text = text;
    }

    public BaseErrorLine(String type, String code, int line, String text) {
        this.type = "E";
        this.code = null;
        this.line = 0;
        this.text = null;
        this.type = type;
        this.code = code;
        this.line = line;
        this.text = text;
    }

    public BaseErrorLine(String code, String txt) {
        this(code != null && code.length() != 0 ? code.substring(0, 1) : "E", code == null ? "" : code, txt);
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getLine() {
        return this.line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "BaseErrorLine{" +
                "type='" + type + '\'' +
                ", code='" + code + '\'' +
                ", line=" + line +
                ", text='" + text + '\'' +
                '}';
    }
}
