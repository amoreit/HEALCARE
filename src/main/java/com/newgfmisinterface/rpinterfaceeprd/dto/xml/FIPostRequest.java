package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
//import com.newgfmisinterface.rpinterfacewss.dto.WSWebInfo;

@XmlRootElement(
        name = "REQUEST"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class FIPostRequest {
    @XmlElement(
            name = "WEBINFO"
    )
    private WSWebInfo webInfo;
    @XmlElement(
            name = "COMP_CODE"
    )
    private String compCode = null;
    @XmlElement(
            name = "ACC_DOC_NO"
    )
    private String accountDocNo = null;
    @XmlElement(
            name = "FISCAL_YEAR"
    )
    private String fiscalYear = null;

    public FIPostRequest() {
    }

    public WSWebInfo getWebInfo() {
        return this.webInfo;
    }

    public void setWebInfo(WSWebInfo webInfo) {
        this.webInfo = webInfo;
    }

    public String getCompCode() {
        return this.compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getAccountDocNo() {
        return this.accountDocNo;
    }

    public void setAccountDocNo(String accountDocNo) {
        this.accountDocNo = accountDocNo;
    }

    public String getFiscalYear() {
        return this.fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }
}
