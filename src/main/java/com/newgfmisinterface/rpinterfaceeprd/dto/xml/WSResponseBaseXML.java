package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class WSResponseBaseXML extends BaseMessage {
    public WSResponseBaseXML() {
    }

    public List<BaseErrorLine> getErrors() {
        return this.getMsgLines();
    }


}