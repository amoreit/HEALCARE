package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ZFIFromDoc {
    @XmlElement(
            name = "HEADER_REFERENCE"
    )
    private String headerReference = null;
    @XmlElement(
            name = "DOC_TYPE"
    )
    private String docType = null;
    @XmlElement(
            name = "COMP_CODE"
    )
    private String compCode = null;
    @XmlElement(
            name = "ACC_DOC_NO"
    )
    private String accountDocNo = null;
    @XmlElement(
            name = "FISCAL_YEAR"
    )
    private String fiscalYear = null;
    @XmlElement(
            name = "FORMID"
    )
    private String formID = null;

    public ZFIFromDoc() {
    }

    public String getHeaderReference() {
        return this.headerReference;
    }

    public void setHeaderReference(String headerReference) {
        this.headerReference = headerReference;
    }

    public String getDocType() {
        return this.docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getCompCode() {
        return this.compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getAccountDocNo() {
        return this.accountDocNo;
    }

    public void setAccountDocNo(String accountDocNo) {
        this.accountDocNo = accountDocNo;
    }

    public String getFiscalYear() {
        return this.fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public String getFormID() {
        return this.formID;
    }

    public void setFormID(String formID) {
        this.formID = formID;
    }
}
