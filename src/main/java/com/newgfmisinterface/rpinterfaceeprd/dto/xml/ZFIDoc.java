package com.newgfmisinterface.rpinterfaceeprd.dto.xml;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
//import th.tgas.publicsector.xml.BaseMessage;
//import th.tgas.publicsector.xml.WSWebInfo;

@XmlRootElement(
        name = "REQUEST"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class ZFIDoc {
    @XmlTransient
    public static final String DOCACTION_PARK = "V";
    @XmlTransient
    public static final String DOCACTION_POST = "P";
    @XmlElement(
            name = "PARK_POST"
    )
    private String parkPost = "";
    @XmlElement(
            name = "FLAG"
    )
    private int flag = 0;
    @XmlElement(
            name = "FORMID"
    )
    private String formID = "";
    @XmlElement(
            name = "AUTODOC"
    )
    private boolean autoDoc = false;
    @XmlElement(
            name = "HEADER"
    )
    private ZFIHeader header;
    @XmlElementWrapper(
            name = "ITEMS"
    )
    @XmlElement(
            name = "ITEM"
    )
    private List<ZFILine> lines = new ArrayList();
    @XmlElement(
            name = "WEBINFO"
    )
    private WSWebInfo webInfo;
    @XmlElement(
            name = "FIRSTHEADER"
    )
    private ZFIFromDoc fromDoc;
    @XmlElementWrapper(
            name = "EXITS_COMPLETES"
    )
    @XmlElement(
            name = "EXITS_COMPLETE"
    )
    private List<String> exitsComplete = new ArrayList();
    @XmlTransient
    private BaseMessage errLines = new BaseMessage();

    public ZFIDoc() {
    }

    public void addExitsComplete(String value) {
        if (!this.exitsComplete.contains(value)) {
            this.exitsComplete.add(value);
        }
    }

    public WSWebInfo getWebInfo() {
        return this.webInfo;
    }

    public void setWebInfo(WSWebInfo webInfo) {
        WSWebInfo obj = new WSWebInfo();
        obj.setIpAddress(webInfo.getIpAddress());
        obj.setFiArea(webInfo.getFiArea());
        obj.setCompCode(webInfo.getCompCode());
        obj.setUserWebOnline(webInfo.getUserWebOnline());
        obj.setPaymentCenter(webInfo.getPaymentCenter());
        this.webInfo = obj;
    }

    public ZFIDoc copy(boolean isInterComp) {
        ZFIDoc docNew = new ZFIDoc();
        docNew.setPARK_POST(this.getPARK_POST());
        docNew.setFLAG(this.getFLAG());
        docNew.setFORMID(this.getFORMID());
        docNew.setAUTODOC(true);
        docNew.setHeader(this.getHeader().copy(isInterComp));
        docNew.setWebInfo(this.getWebInfo());
        return docNew;
    }

    public boolean isError() {
        return this.errLines.isError();
    }

    public String getPARK_POST() {
        return this.parkPost;
    }

    public void setPARK_POST(String pARK_POST) {
        this.parkPost = pARK_POST;
    }

    public int getFLAG() {
        return this.flag;
    }

    public void setFLAG(int fLAG) {
        this.flag = fLAG;
    }

    public String getFORMID() {
        return this.formID;
    }

    public void setFORMID(String fORMID) {
        this.formID = fORMID;
    }

    public boolean isAUTODOC() {
        return this.autoDoc;
    }

    public void setAUTODOC(boolean aUTODOC) {
        this.autoDoc = aUTODOC;
    }

    public List<ZFILine> getLines() {
        return this.lines;
    }

    public void setLines(List<ZFILine> lines) {
        this.lines = lines;
    }

    public void addLine(ZFILine line) {
        this.lines.add(line);
    }

    public void addLine(int position, ZFILine line) {
        this.lines.add(position, line);
    }

    public ZFIHeader getHeader() {
        return this.header;
    }

    public void setHeader(ZFIHeader header) {
        this.header = header;
    }

    public ZFIFromDoc getFromDoc() {
        return this.fromDoc;
    }

    public void setFromDoc(ZFIFromDoc fromDoc) {
        this.fromDoc = fromDoc;
    }

    public List<String> getExitsComplete() {
        return this.exitsComplete;
    }

    public void setExitsComplete(List<String> exits) {
        this.exitsComplete = exits;
    }

    public BaseMessage getErrLines() {
        return this.errLines;
    }

    public void setErrLines(BaseMessage errLines) {
        this.errLines = errLines;
    }
}

