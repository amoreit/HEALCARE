package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

@XmlAccessorType(XmlAccessType.FIELD)
public class BaseMessage {
    @XmlElementWrapper(
            name = "ERRORS"
    )
    @XmlElement(
            name = "ERROR"
    )
    protected List<BaseErrorLine> msgLines = new ArrayList();

    public BaseMessage() {
    }

    public List<BaseErrorLine> getMsgLines() {
        return this.msgLines;
    }

    public void setMsgLines(List<BaseErrorLine> msgLines) {
        this.msgLines = msgLines;
    }

    public void addMsgAll(List<BaseErrorLine> lstObj) {
        Iterator var3 = lstObj.iterator();

        while(var3.hasNext()) {
            BaseErrorLine obj = (BaseErrorLine)var3.next();
            this.msgLines.add(obj);
        }

    }

    public void addMsgAll(BaseMessage objMsg) {
        Iterator var3 = objMsg.getMsgLines().iterator();

        while(var3.hasNext()) {
            BaseErrorLine obj = (BaseErrorLine)var3.next();
            this.msgLines.add(obj);
        }

    }

    public boolean removeDuplicate() {
        List<BaseErrorLine> newLines = new ArrayList();
        Iterator var3 = this.msgLines.iterator();

        while(var3.hasNext()) {
            BaseErrorLine msgLine = (BaseErrorLine)var3.next();
            boolean isDup = false;
            Iterator var6 = newLines.iterator();

            while(var6.hasNext()) {
                BaseErrorLine nLine = (BaseErrorLine)var6.next();
                if (msgLine.getCode().equals(nLine.getCode()) && msgLine.getText().equals(nLine.getText())) {
                    isDup = true;
                }
            }

            if (!isDup) {
                newLines.add(msgLine);
            }
        }

        this.msgLines = newLines;
        return false;
    }

    public void sortMessage() {
        this.msgLines.sort(Comparator.comparing(BaseErrorLine::getType));
    }

    public boolean isError() {
        Iterator var2 = this.msgLines.iterator();

        while(var2.hasNext()) {
            BaseErrorLine msgLine = (BaseErrorLine)var2.next();
            if ("E".equals(msgLine.getType())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return "BaseMessage{" +
                "msgLines=" + msgLines +
                '}';
    }
}
