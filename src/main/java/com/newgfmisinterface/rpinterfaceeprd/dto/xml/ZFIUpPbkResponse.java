package com.newgfmisinterface.rpinterfaceeprd.dto.xml;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(
        name = "RESPONSE"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class ZFIUpPbkResponse extends BaseMessage {
    @XmlElement(
            name = "DOC_TYPE"
    )
    private String docType = null;
    @XmlElement(
            name = "COMP_CODE"
    )
    private String compCode = null;
    @XmlElement(
            name = "ACC_DOC_NO"
    )
    private String accountDocNo = null;
    @XmlElement(
            name = "FISCAL_YEAR"
    )
    private String fiscalYear = null;
    @XmlElement(
            name = "APPROVAL"
    )
    private boolean approval = false;
    @XmlElement(
            name = "PAYMENT_BLOCK"
    )
    private String paymentBlock = null;
    @XmlElement(
            name = "REASON"
    )
    private String reason = null;
    @XmlElement(
            name = "USERWEBONLINE"
    )
    private String userWebOnline = null;
    @XmlElement(
            name = "DOC_STATUS"
    )
    private String docStatus = null;
    @XmlElement(
            name = "SUCCESS"
    )
    private boolean success = false;
    @XmlElement(
            name = "REV_ACC_DOC_NO"
    )
    private String revAccountDocNo = null;
    @XmlElement(
            name = "REV_FISCAL_YEAR"
    )
    private String revFiscalYear = null;
    @XmlElement(
            name = "REV_DOC_TYPE"
    )
    private String revDocType = null;

    public ZFIUpPbkResponse() {
    }

    public String getDocType() {
        return this.docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getCompCode() {
        return this.compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getAccountDocNo() {
        return this.accountDocNo;
    }

    public void setAccountDocNo(String accountDocNo) {
        this.accountDocNo = accountDocNo;
    }

    public String getFiscalYear() {
        return this.fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public boolean isApproval() {
        return this.approval;
    }

    public void setApproval(boolean approval) {
        this.approval = approval;
    }

    public String getPaymentBlock() {
        return this.paymentBlock;
    }

    public void setPaymentBlock(String paymentBlock) {
        this.paymentBlock = paymentBlock;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUserWebOnline() {
        return this.userWebOnline;
    }

    public void setUserWebOnline(String userWebOnline) {
        this.userWebOnline = userWebOnline;
    }

    public String getDocStatus() {
        return this.docStatus;
    }

    public void setDocStatus(String docStatus) {
        this.docStatus = docStatus;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getRevAccountDocNo() {
        return this.revAccountDocNo;
    }

    public void setRevAccountDocNo(String revAccountDocNo) {
        this.revAccountDocNo = revAccountDocNo;
    }

    public String getRevFiscalYear() {
        return this.revFiscalYear;
    }

    public void setRevFiscalYear(String revFiscalYear) {
        this.revFiscalYear = revFiscalYear;
    }

    public String getRevDocType() {
        return this.revDocType;
    }

    public void setRevDocType(String revDocType) {
        this.revDocType = revDocType;
    }
}
