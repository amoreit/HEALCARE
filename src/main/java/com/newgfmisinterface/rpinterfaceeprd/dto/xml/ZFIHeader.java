package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.newgfmisinterface.rpinterfaceeprd.adapter.DateTimeAdapter;
import com.newgfmisinterface.rpinterfaceeprd.adapter.TimestampAdapter;


@XmlAccessorType(XmlAccessType.FIELD)
public class ZFIHeader {
    @XmlElement(
            name = "DOC_TYPE"
    )
    private String docType = null;
    @XmlElement(
            name = "COMP_CODE"
    )
    private String compCode = null;
    @XmlElement(
            name = "DATE_DOC"
    )
    @XmlJavaTypeAdapter(TimestampAdapter.class)
    private Timestamp dateDoc = null;
    @XmlElement(
            name = "DATE_ACCT"
    )
    @XmlJavaTypeAdapter(TimestampAdapter.class)
    private Timestamp dateAcct = null;
    @XmlElement(
            name = "PERIOD"
    )
    private int period = 0;
    @XmlElement(
            name = "CURRENCY"
    )
    private String currency = null;
    @XmlElement(
            name = "AMOUNT"
    )
    private BigDecimal amount = null;
    @XmlElement(
            name = "PAYMENT_CENTER"
    )
    private String paymentCenter = null;
    @XmlElement(
            name = "BR_DOC_NO"
    )
    private String brDocNo = null;
    @XmlElement(
            name = "PURCHAS_DOC_NO"
    )
    private String poDocNo = null;
    @XmlElement(
            name = "INV_DOC_NO"
    )
    private String invDocNo = null;
    @XmlElement(
            name = "REV_INV_DOC_NO"
    )
    private String revInvDocNo = null;
    @XmlElement(
            name = "ACC_DOC_NO"
    )
    private String accDocNo = null;
    @XmlElement(
            name = "REV_ACC_DOC_NO"
    )
    private String revAccDocNo = null;
    @XmlElement(
            name = "FISCAL_YEAR"
    )
    private String fiscalYear = null;
    @XmlElement(
            name = "REV_FISCAL_YEAR"
    )
    private String revFiscalYear = null;
    @XmlElement(
            name = "PAYMENT_METHOD"
    )
    private String paymentMethod = null;
    @XmlElement(
            name = "COST_CENTER1"
    )
    private String costCenter1 = null;
    @XmlElement(
            name = "COST_CENTER2"
    )
    private String costCenter2 = null;
    @XmlElement(
            name = "HEADER_REFERENCE"
    )
    private String headerReference = null;
    @XmlElement(
            name = "DOC_HEADER_TEXT"
    )
    private String docHeaderText = null;
    @XmlElement(
            name = "HEADER_REFERENCE2"
    )
    private String headerReference2 = null;
    @XmlElement(
            name = "REV_DATE_ACCT"
    )
    @XmlJavaTypeAdapter(TimestampAdapter.class)
    private Timestamp revDateAcct = null;
    @XmlElement(
            name = "REV_REASON"
    )
    private String revReason = null;
    @XmlElement(
            name = "CREATED"
    )
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private Timestamp created = null;
    @XmlElement(
            name = "USERPARK"
    )
    private String userPark = null;
    @XmlElement(
            name = "USERPOST"
    )
    private String userPost = null;
    @XmlElement(
            name = "ORIGINAL_DOC"
    )
    private String originalDoc = null;
    @XmlElement(
            name = "REF_INTER_DOC_NO"
    )
    private String refInterDocNo = null;
    @XmlElement(
            name = "REF_INTER_COMP_CODE"
    )
    private String refInterCompCode = null;
    @XmlElement(
            name = "REF_AUTOGEN"
    )
    private String refAutoGen = null;
    @XmlElement(
            name = "DOC_STATUS"
    )
    private String docStatus = null;

    public ZFIHeader() {
    }

    public ZFIHeader copy(boolean isInterComp) {
        ZFIHeader newObj = new ZFIHeader();
        newObj.setDocType(this.getDocType());
        newObj.setCompCode(this.getCompCode());
        newObj.setDateDoc(this.getDateDoc());
        newObj.setDateAcct(this.getDateAcct());
        newObj.setHeaderReference(this.getHeaderReference());
        newObj.setCurrency(this.getCurrency());
        newObj.setRevDateAcct(this.getRevDateAcct());
        newObj.setRevReason(this.getRevReason());
        newObj.setPaymentCenter(this.getPaymentCenter());
        newObj.setCostCenter1(this.getCostCenter1());
        newObj.setCostCenter2(this.getCostCenter2());
        newObj.setPeriod(this.getPeriod());
        newObj.setAccDocNo(this.getAccDocNo());
        newObj.setRevAccDocNo(this.getRevAccDocNo());
        newObj.setFiscalYear(this.getFiscalYear());
        newObj.setRevFiscalYear(this.getRevFiscalYear());
        newObj.setDocHeaderText(this.getDocHeaderText());
        newObj.setOriginalDoc(this.getOriginalDoc());
        newObj.setRefAutoGen(this.getRefAutoGen());
        if (isInterComp) {
            newObj.setRefInterDocNo(this.getRefInterDocNo());
            newObj.setRefInterCompCode(this.getRefInterCompCode());
        }

        return newObj;
    }

    public String getDocType() {
        return this.docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getCompCode() {
        return this.compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public Timestamp getDateDoc() {
        return this.dateDoc;
    }

    public void setDateDoc(Timestamp dateDoc) {
        this.dateDoc = dateDoc;
    }

    public Timestamp getDateAcct() {
        return this.dateAcct;
    }

    public void setDateAcct(Timestamp dateAcct) {
        this.dateAcct = dateAcct;
    }

    public int getPeriod() {
        return this.period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaymentCenter() {
        return this.paymentCenter;
    }

    public void setPaymentCenter(String paymentCenter) {
        this.paymentCenter = paymentCenter;
    }

    public String getBrDocNo() {
        return this.brDocNo;
    }

    public void setBrDocNo(String brDocNo) {
        this.brDocNo = brDocNo;
    }

    public String getPoDocNo() {
        return this.poDocNo;
    }

    public void setPoDocNo(String poDocNo) {
        this.poDocNo = poDocNo;
    }

    public String getInvDocNo() {
        return this.invDocNo;
    }

    public void setInvDocNo(String invDocNo) {
        this.invDocNo = invDocNo;
    }

    public String getRevInvDocNo() {
        return this.revInvDocNo;
    }

    public void setRevInvDocNo(String revInvDocNo) {
        this.revInvDocNo = revInvDocNo;
    }

    public String getAccDocNo() {
        return this.accDocNo;
    }

    public void setAccDocNo(String accDocNo) {
        this.accDocNo = accDocNo;
    }

    public String getRevAccDocNo() {
        return this.revAccDocNo;
    }

    public void setRevAccDocNo(String revAccDocNo) {
        this.revAccDocNo = revAccDocNo;
    }

    public String getFiscalYear() {
        return this.fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public String getRevFiscalYear() {
        return this.revFiscalYear;
    }

    public void setRevFiscalYear(String revFiscalYear) {
        this.revFiscalYear = revFiscalYear;
    }

    public String getPaymentMethod() {
        return this.paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCostCenter1() {
        return this.costCenter1;
    }

    public void setCostCenter1(String costCenter1) {
        this.costCenter1 = costCenter1;
    }

    public String getCostCenter2() {
        return this.costCenter2;
    }

    public void setCostCenter2(String costCenter2) {
        this.costCenter2 = costCenter2;
    }

    public String getHeaderReference() {
        return this.headerReference;
    }

    public void setHeaderReference(String headerReference) {
        this.headerReference = headerReference;
    }

    public String getDocHeaderText() {
        return this.docHeaderText;
    }

    public void setDocHeaderText(String docHeaderText) {
        this.docHeaderText = docHeaderText;
    }

    public String getHeaderReference2() {
        return this.headerReference2;
    }

    public void setHeaderReference2(String hdReference2) {
        this.headerReference2 = hdReference2;
    }

    public Timestamp getRevDateAcct() {
        return this.revDateAcct;
    }

    public void setRevDateAcct(Timestamp revDatePost) {
        this.revDateAcct = revDatePost;
    }

    public String getRevReason() {
        return this.revReason;
    }

    public void setRevReason(String revReasonPost) {
        this.revReason = revReasonPost;
    }

    public String getOriginalDoc() {
        return this.originalDoc;
    }

    public void setOriginalDoc(String originalDoc) {
        this.originalDoc = originalDoc;
    }

    public Timestamp getCreated() {
        return this.created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public String getUserPark() {
        return this.userPark;
    }

    public void setUserPark(String userPark) {
        this.userPark = userPark;
    }

    public String getUserPost() {
        return this.userPost;
    }

    public void setUserPost(String userPost) {
        this.userPost = userPost;
    }

    public String getRefInterDocNo() {
        return this.refInterDocNo;
    }

    public void setRefInterDocNo(String refInterDocNo) {
        this.refInterDocNo = refInterDocNo;
    }

    public String getRefInterCompCode() {
        return this.refInterCompCode;
    }

    public void setRefInterCompCode(String refInterCompCode) {
        this.refInterCompCode = refInterCompCode;
    }

    public String getRefAutoGen() {
        return this.refAutoGen;
    }

    public void setRefAutoGen(String refAutoGen) {
        this.refAutoGen = refAutoGen;
    }

    public String getDocStatus() {
        return this.docStatus;
    }

    public void setDocStatus(String docStatus) {
        this.docStatus = docStatus;
    }
}

//@XmlAccessorType(XmlAccessType.FIELD)
//public class ZFIHeader {
//    @XmlElement(name = "DOC_TYPE")
//    private String docType = null;
//    @XmlElement(name = "COMP_CODE")
//    private String compCode = null;
//
//    @XmlElement(name = "DATE_DOC")
//    private String dateDoc = null;
//
//    @XmlElement(name = "DATE_ACCT")
//    private String dateAcct = null;
//
//    @XmlElement(name = "PERIOD")
//    private int period = 0;
//    @XmlElement(name = "CURRENCY")
//    private String currency = null;
//    @XmlElement(name = "PAYMENT_CENTER")
//    private String paymentCenter = null;
//    @XmlElement(name = "ACC_DOC_NO")
//    private String accDocNo = null;
//    @XmlElement(name = "FISCAL_YEAR")
//    private String fiscalYear = null;
//    @XmlElement(name = "COST_CENTER1")
//    private String costCenter1 = null;
//    @XmlElement(name = "COST_CENTER2")
//    private String costCenter2 = null;
//    @XmlElement(name = "HEADER_REFERENCE")
//    private String headerReference = null;
//    @XmlElement(name = "DOC_HEADER_TEXT")
//    private String docHeaderText = null;
//
//    @XmlElement(name = "CREATED")
//    @XmlJavaTypeAdapter(DateTimeAdapter.class)
//    private Timestamp created = null;
//
//    @XmlElement(name = "USERPARK")
//    private String userPark = null;
//    @XmlElement(name = "ORIGINAL_DOC")
//    private String originalDoc = null;
//    @XmlElement(name = "REF_INTER_DOC_NO")
//    private String refInterDocNo = null;
//    @XmlElement(name = "REF_INTER_COMP_CODE")
//    private String refInterCompCode = null;
//    @XmlElement(name = "REF_AUTOGEN")
//    private String refAutoGen = null;
//    @XmlElement(name = "DOC_STATUS")
//    private String docStatus = null;
//
//    public ZFIHeader() {
//    }
//
//    public ZFIHeader(String docType, String compCode, String dateDoc, String dateAcct, int period, String currency, String paymentCenter, String accDocNo, String fiscalYear, String costCenter1, String costCenter2, String headerReference, String docHeaderText, Timestamp created, String userPark, String originalDoc, String refInterDocNo, String refInterCompCode, String refAutoGen, String docStatus) {
//        this.docType = docType;
//        this.compCode = compCode;
//        this.dateDoc = dateDoc;
//        this.dateAcct = dateAcct;
//        this.period = period;
//        this.currency = currency;
//        this.paymentCenter = paymentCenter;
//        this.accDocNo = accDocNo;
//        this.fiscalYear = fiscalYear;
//        this.costCenter1 = costCenter1;
//        this.costCenter2 = costCenter2;
//        this.headerReference = headerReference;
//        this.docHeaderText = docHeaderText;
//        this.created = created;
//        this.userPark = userPark;
//        this.originalDoc = originalDoc;
//        this.refInterDocNo = refInterDocNo;
//        this.refInterCompCode = refInterCompCode;
//        this.refAutoGen = refAutoGen;
//        this.docStatus = docStatus;
//    }
//
//    public String getDocType() {
//        return docType;
//    }
//
//    public void setDocType(String docType) {
//        this.docType = docType;
//    }
//
//    public String getCompCode() {
//        return compCode;
//    }
//
//    public void setCompCode(String compCode) {
//        this.compCode = compCode;
//    }
//
//    public String getDateDoc() {
//        return dateDoc;
//    }
//
//    public void setDateDoc(String dateDoc) {
//        this.dateDoc = dateDoc;
//    }
//
//    public String getDateAcct() {
//        return dateAcct;
//    }
//
//    public void setDateAcct(String dateAcct) {
//        this.dateAcct = dateAcct;
//    }
//
//    public int getPeriod() {
//        return period;
//    }
//
//    public void setPeriod(int period) {
//        this.period = period;
//    }
//
//    public String getCurrency() {
//        return currency;
//    }
//
//    public void setCurrency(String currency) {
//        this.currency = currency;
//    }
//
//    public String getPaymentCenter() {
//        return paymentCenter;
//    }
//
//    public void setPaymentCenter(String paymentCenter) {
//        this.paymentCenter = paymentCenter;
//    }
//
//    public String getAccDocNo() {
//        return accDocNo;
//    }
//
//    public void setAccDocNo(String accDocNo) {
//        this.accDocNo = accDocNo;
//    }
//
//    public String getFiscalYear() {
//        return fiscalYear;
//    }
//
//    public void setFiscalYear(String fiscalYear) {
//        this.fiscalYear = fiscalYear;
//    }
//
//    public String getCostCenter1() {
//        return costCenter1;
//    }
//
//    public void setCostCenter1(String costCenter1) {
//        this.costCenter1 = costCenter1;
//    }
//
//    public String getCostCenter2() {
//        return costCenter2;
//    }
//
//    public void setCostCenter2(String costCenter2) {
//        this.costCenter2 = costCenter2;
//    }
//
//    public String getHeaderReference() {
//        return headerReference;
//    }
//
//    public void setHeaderReference(String headerReference) {
//        this.headerReference = headerReference;
//    }
//
//    public String getDocHeaderText() {
//        return docHeaderText;
//    }
//
//    public void setDocHeaderText(String docHeaderText) {
//        this.docHeaderText = docHeaderText;
//    }
//
//    public Timestamp getCreated() {
//        return created;
//    }
//
//    public void setCreated(Timestamp created) {
//        this.created = created;
//    }
//
//    public String getUserPark() {
//        return userPark;
//    }
//
//    public void setUserPark(String userPark) {
//        this.userPark = userPark;
//    }
//
//    public String getOriginalDoc() {
//        return originalDoc;
//    }
//
//    public void setOriginalDoc(String originalDoc) {
//        this.originalDoc = originalDoc;
//    }
//
//    public String getRefInterDocNo() {
//        return refInterDocNo;
//    }
//
//    public void setRefInterDocNo(String refInterDocNo) {
//        this.refInterDocNo = refInterDocNo;
//    }
//
//    public String getRefInterCompCode() {
//        return refInterCompCode;
//    }
//
//    public void setRefInterCompCode(String refInterCompCode) {
//        this.refInterCompCode = refInterCompCode;
//    }
//
//    public String getRefAutoGen() {
//        return refAutoGen;
//    }
//
//    public void setRefAutoGen(String refAutoGen) {
//        this.refAutoGen = refAutoGen;
//    }
//
//    public String getDocStatus() {
//        return docStatus;
//    }
//
//    public void setDocStatus(String docStatus) {
//        this.docStatus = docStatus;
//    }
//
//    @Override
//    public String toString() {
//        return "HeaderDto{" +
//                "docType='" + docType + '\'' +
//                ", compCode='" + compCode + '\'' +
//                ", dateDoc=" + dateDoc +
//                ", dateAcct=" + dateAcct +
//                ", period=" + period +
//                ", currency='" + currency + '\'' +
//                ", paymentCenter='" + paymentCenter + '\'' +
//                ", accDocNo='" + accDocNo + '\'' +
//                ", fiscalYear='" + fiscalYear + '\'' +
//                ", costCenter1='" + costCenter1 + '\'' +
//                ", costCenter2='" + costCenter2 + '\'' +
//                ", headerReference='" + headerReference + '\'' +
//                ", docHeaderText='" + docHeaderText + '\'' +
//                ", created=" + created +
//                ", userPark='" + userPark + '\'' +
//                ", originalDoc='" + originalDoc + '\'' +
//                ", refInterDocNo='" + refInterDocNo + '\'' +
//                ", refInterCompCode='" + refInterCompCode + '\'' +
//                ", refAutoGen='" + refAutoGen + '\'' +
//                ", docStatus='" + docStatus + '\'' +
//                '}';
//    }
//}
