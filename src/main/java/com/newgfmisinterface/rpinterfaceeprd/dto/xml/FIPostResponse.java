package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(
        name = "RESPONSE"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class FIPostResponse extends FIResponse2 {
    @XmlElement(
            name = "STATUS"
    )
    private String status = null;

    public FIPostResponse() {
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static enum Status {
        SUCCESS("S"),
        ERROR("E");

        private final String code;

        private Status(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

    @Override
    public String toString() {
        return "FIPostResponse{" +
                "status='" + status + '\'' +
                '}';
    }
}