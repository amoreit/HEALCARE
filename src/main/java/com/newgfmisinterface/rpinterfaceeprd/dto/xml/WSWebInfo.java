package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class WSWebInfo {
    @XmlElement(
            name = "IPADDRESS"
    )
    private String ipAddress = "";
    @XmlElement(
            name = "USERWEBONLINE"
    )
    private String userWebOnline = "";
    @XmlElement(
            name = "FI_AREA"
    )
    private String fiArea = "";
    @XmlElement(
            name = "COMP_CODE"
    )
    private String compCode = "";
    @XmlElement(
            name = "PAYMENT_CENTER"
    )
    private String paymentCenter = "";
    @XmlElement(
            name = "AUTH_PAYMENT_CENTER"
    )
    private List<String> authPaymentCenter = new ArrayList();
    @XmlElement(
            name = "AUTH_COST_CENTER"
    )
    private List<String> authCostCenter = new ArrayList();
    @XmlElement(
            name = "AUTH_FI_AREA"
    )
    private List<String> authFIArea = new ArrayList();
    @XmlElement(
            name = "AUTH_COMP_CODE"
    )
    private List<String> authCompanyCode = new ArrayList();

    public WSWebInfo() {
    }

    public String getIpAddress() {
        return this.ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserWebOnline() {
        return this.userWebOnline;
    }

    public void setUserWebOnline(String userWebOnline) {
        this.userWebOnline = userWebOnline;
    }

    public String getFiArea() {
        return this.fiArea;
    }

    public void setFiArea(String fiArea) {
        this.fiArea = fiArea;
    }

    public String getCompCode() {
        return this.compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getPaymentCenter() {
        return this.paymentCenter;
    }

    public void setPaymentCenter(String paymentCenter) {
        this.paymentCenter = paymentCenter;
    }

    public List<String> getAuthPaymentCenter() {
        return this.authPaymentCenter;
    }

    public void setAuthPaymentCenter(List<String> authPaymentCenter) {
        this.authPaymentCenter = authPaymentCenter;
    }

    public List<String> getAuthCostCenter() {
        return this.authCostCenter;
    }

    public void setAuthCostCenter(List<String> authCostCenter) {
        this.authCostCenter = authCostCenter;
    }

    public List<String> getAuthFIArea() {
        return this.authFIArea;
    }

    public void setAuthFIArea(List<String> authArea) {
        this.authFIArea = authArea;
    }

    public List<String> getAuthCompanyCode() {
        return this.authCompanyCode;
    }

    public void setAuthCompanyCode(List<String> authCompanyCode) {
        this.authCompanyCode = authCompanyCode;
    }
}
