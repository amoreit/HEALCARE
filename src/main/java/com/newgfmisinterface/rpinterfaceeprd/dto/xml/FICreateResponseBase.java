package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class FICreateResponseBase extends WSResponseBaseXML {
    @XmlElement(
            name = "COMP_CODE"
    )
    private String compCode = "";
    @XmlElement(
            name = "ACC_DOC_NO"
    )
    private String accDocNo = "";
    @XmlElement(
            name = "FISCAL_YEAR"
    )
    private String fiscalYear = "";
    @XmlElement(
            name = "DOC_TYPE"
    )
    private String docType = "";
    @XmlElement(
            name = "DOC_STATUS"
    )
    private String docStatus = "";
    @XmlElement(
            name = "DOC_STATUS_NAME"
    )
    private String docStatusName = "";
    @XmlElement(
            name = "REV_ACC_DOC_NO"
    )
    private String revAccDocNo = "";
    @XmlElement(
            name = "REV_FISCAL_YEAR"
    )
    private String revFiscalYear = "";
    @XmlElement(
            name = "REV_DOC_TYPE"
    )
    private String revDocType = "";
    @XmlElement(
            name = "REMARK"
    )
    private String remark = "";

    public FICreateResponseBase() {
    }

    public String getCompCode() {
        return this.compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getAccDocNo() {
        return this.accDocNo;
    }

    public void setAccDocNo(String accDocNo) {
        this.accDocNo = accDocNo;
    }

    public String getFiscalYear() {
        return this.fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public String getDocType() {
        return this.docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocStatus() {
        return this.docStatus;
    }

    public void setDocStatus(String docStatus) {
        this.docStatus = docStatus;
    }

    public String getDocStatusName() {
        return this.docStatusName;
    }

    public void setDocStatusName(String docStatusName) {
        this.docStatusName = docStatusName;
    }

    public String getRevAccDocNo() {
        return this.revAccDocNo;
    }

    public void setRevAccDocNo(String revAccDocNo) {
        this.revAccDocNo = revAccDocNo;
    }

    public String getRevFiscalYear() {
        return this.revFiscalYear;
    }

    public void setRevFiscalYear(String revFiscalYear) {
        this.revFiscalYear = revFiscalYear;
    }

    public String getRevDocType() {
        return this.revDocType;
    }

    public void setRevDocType(String revDocType) {
        this.revDocType = revDocType;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "FICreateResponseBase{" +
                "compCode='" + compCode + '\'' +
                ", accDocNo='" + accDocNo + '\'' +
                ", fiscalYear='" + fiscalYear + '\'' +
                ", docType='" + docType + '\'' +
                ", docStatus='" + docStatus + '\'' +
                ", docStatusName='" + docStatusName + '\'' +
                ", revAccDocNo='" + revAccDocNo + '\'' +
                ", revFiscalYear='" + revFiscalYear + '\'' +
                ", revDocType='" + revDocType + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
