package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(
//        name = "RESPONSE"
//)
@XmlAccessorType(XmlAccessType.FIELD)
public class FIResponse2 extends FICreateResponseBase {
    @XmlElementWrapper(
            name = "AUTODOCS"
    )
    @XmlElement(
            name = "AUTODOC"
    )
    private List<FICreateResponseBase> autoDoc = new ArrayList();

    public FIResponse2() {
    }

    public List<FICreateResponseBase> getAutoDoc() {
        return this.autoDoc;
    }

    public void setAutoDoc(List<FICreateResponseBase> autoDoc) {
        this.autoDoc = autoDoc;
    }

    @Override
    public String toString() {
        return "FIResponse2{" +
                "autoDoc=" + autoDoc +
                '}';
    }
}
