package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FI")
@XmlAccessorType(XmlAccessType.FIELD)
public class MainFiDto {
    @XmlElement(name = "ID")
    private String id;
    @XmlElement(name = "TYPE")
    private String type;
    @XmlElement(name = "DATATYPE")
    private String dataType;
    @XmlElement(name = "FROM")
    private String from;
    @XmlElement(name = "TO")
    private String to;
    @XmlElement(name = "DATA")
    private RequestDto data;

    public MainFiDto() {
    }

    public MainFiDto(String id, String type, String dataType, String from, String to, RequestDto data) {
        this.id = id;
        this.type = type;
        this.dataType = dataType;
        this.from = from;
        this.to = to;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public RequestDto getData() {
        return data;
    }

    public void setData(RequestDto data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "mainFiDto{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", dataType='" + dataType + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", data=" + data +
                '}';
    }
}
