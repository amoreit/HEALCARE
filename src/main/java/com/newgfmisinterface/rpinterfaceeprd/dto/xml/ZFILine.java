package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
public class ZFILine {
    private String POSTING_KEY = "";
    private String ACC_TYPE = "";
    private String DR_CR = "";
    private String GL_ACCOUNT = "";
    private String FI_AREA = "";
    private String COST_CENTER = "";
    private String FUND_SOURCE = "";
    private String BG_CODE = "";
    private String BG_ACTIVITY = "";
    private BigDecimal AMOUNT = null;
    private String REFERENCE3 = "";
    private String ASSIGNMENT = "";
    private int BR_LINE = 0;
    private String PAYMENT_CENTER = "";
    private String TRADING_PARTNER = "";
    private String COMP_CODE = "";
    private int PO_LINE = 0;
    private String MR_LINE = "";
    private String INCOME = "";
    private boolean AUTOGEN = false;
    private boolean IS_WTX = false;
    private int LINE;

    public ZFILine() {
    }

    public ZFILine(String POSTING_KEY, String ACC_TYPE, String DR_CR, String GL_ACCOUNT, String FI_AREA, String COST_CENTER, String FUND_SOURCE, String BG_CODE, String BG_ACTIVITY, BigDecimal AMOUNT, String REFERENCE3, String ASSIGNMENT, int BR_LINE, String PAYMENT_CENTER, String TRADING_PARTNER, String COMP_CODE, int PO_LINE, String MR_LINE, String INCOME, boolean AUTOGEN, boolean IS_WTX, int LINE) {
        this.POSTING_KEY = POSTING_KEY;
        this.ACC_TYPE = ACC_TYPE;
        this.DR_CR = DR_CR;
        this.GL_ACCOUNT = GL_ACCOUNT;
        this.FI_AREA = FI_AREA;
        this.COST_CENTER = COST_CENTER;
        this.FUND_SOURCE = FUND_SOURCE;
        this.BG_CODE = BG_CODE;
        this.BG_ACTIVITY = BG_ACTIVITY;
        this.AMOUNT = AMOUNT;
        this.REFERENCE3 = REFERENCE3;
        this.ASSIGNMENT = ASSIGNMENT;
        this.BR_LINE = BR_LINE;
        this.PAYMENT_CENTER = PAYMENT_CENTER;
        this.TRADING_PARTNER = TRADING_PARTNER;
        this.COMP_CODE = COMP_CODE;
        this.PO_LINE = PO_LINE;
        this.MR_LINE = MR_LINE;
        this.INCOME = INCOME;
        this.AUTOGEN = AUTOGEN;
        this.IS_WTX = IS_WTX;
        this.LINE = LINE;
    }

    public String getPOSTING_KEY() {
        return POSTING_KEY;
    }

    public void setPOSTING_KEY(String POSTING_KEY) {
        this.POSTING_KEY = POSTING_KEY;
    }

    public String getACC_TYPE() {
        return ACC_TYPE;
    }

    public void setACC_TYPE(String ACC_TYPE) {
        this.ACC_TYPE = ACC_TYPE;
    }

    public String getDR_CR() {
        return DR_CR;
    }

    public void setDR_CR(String DR_CR) {
        this.DR_CR = DR_CR;
    }

    public String getGL_ACCOUNT() {
        return GL_ACCOUNT;
    }

    public void setGL_ACCOUNT(String GL_ACCOUNT) {
        this.GL_ACCOUNT = GL_ACCOUNT;
    }

    public String getFI_AREA() {
        return FI_AREA;
    }

    public void setFI_AREA(String FI_AREA) {
        this.FI_AREA = FI_AREA;
    }

    public String getCOST_CENTER() {
        return COST_CENTER;
    }

    public void setCOST_CENTER(String COST_CENTER) {
        this.COST_CENTER = COST_CENTER;
    }

    public String getFUND_SOURCE() {
        return FUND_SOURCE;
    }

    public void setFUND_SOURCE(String FUND_SOURCE) {
        this.FUND_SOURCE = FUND_SOURCE;
    }

    public String getBG_CODE() {
        return BG_CODE;
    }

    public void setBG_CODE(String BG_CODE) {
        this.BG_CODE = BG_CODE;
    }

    public String getBG_ACTIVITY() {
        return BG_ACTIVITY;
    }

    public void setBG_ACTIVITY(String BG_ACTIVITY) {
        this.BG_ACTIVITY = BG_ACTIVITY;
    }

    public BigDecimal getAMOUNT() {
        return AMOUNT;
    }

    public void setAMOUNT(BigDecimal AMOUNT) {
        this.AMOUNT = AMOUNT;
    }

    public String getREFERENCE3() {
        return REFERENCE3;
    }

    public void setREFERENCE3(String REFERENCE3) {
        this.REFERENCE3 = REFERENCE3;
    }

    public String getASSIGNMENT() {
        return ASSIGNMENT;
    }

    public void setASSIGNMENT(String ASSIGNMENT) {
        this.ASSIGNMENT = ASSIGNMENT;
    }

    public int getBR_LINE() {
        return BR_LINE;
    }

    public void setBR_LINE(int BR_LINE) {
        this.BR_LINE = BR_LINE;
    }

    public String getPAYMENT_CENTER() {
        return PAYMENT_CENTER;
    }

    public void setPAYMENT_CENTER(String PAYMENT_CENTER) {
        this.PAYMENT_CENTER = PAYMENT_CENTER;
    }

    public String getTRADING_PARTNER() {
        return TRADING_PARTNER;
    }

    public void setTRADING_PARTNER(String TRADING_PARTNER) {
        this.TRADING_PARTNER = TRADING_PARTNER;
    }

    public String getCOMP_CODE() {
        return COMP_CODE;
    }

    public void setCOMP_CODE(String COMP_CODE) {
        this.COMP_CODE = COMP_CODE;
    }

    public int getPO_LINE() {
        return PO_LINE;
    }

    public void setPO_LINE(int PO_LINE) {
        this.PO_LINE = PO_LINE;
    }

    public String getMR_LINE() {
        return MR_LINE;
    }

    public void setMR_LINE(String MR_LINE) {
        this.MR_LINE = MR_LINE;
    }

    public String getINCOME() {
        return INCOME;
    }

    public void setINCOME(String INCOME) {
        this.INCOME = INCOME;
    }

    public boolean getAUTOGEN() {
        return AUTOGEN;
    }

    public void setAUTOGEN(boolean AUTOGEN) {
        this.AUTOGEN = AUTOGEN;
    }

    public boolean getIS_WTX() {
        return IS_WTX;
    }

    public void setIS_WTX(boolean IS_WTX) {
        this.IS_WTX = IS_WTX;
    }

    public int getLINE() {
        return LINE;
    }

    public void setLINE(int LINE) {
        this.LINE = LINE;
    }

    @Override
    public String toString() {
        return "LineDto{" +
                "POSTING_KEY='" + POSTING_KEY + '\'' +
                ", ACC_TYPE='" + ACC_TYPE + '\'' +
                ", DR_CR='" + DR_CR + '\'' +
                ", GL_ACCOUNT='" + GL_ACCOUNT + '\'' +
                ", FI_AREA='" + FI_AREA + '\'' +
                ", COST_CENTER='" + COST_CENTER + '\'' +
                ", FUND_SOURCE='" + FUND_SOURCE + '\'' +
                ", BG_CODE='" + BG_CODE + '\'' +
                ", BG_ACTIVITY='" + BG_ACTIVITY + '\'' +
                ", AMOUNT=" + AMOUNT +
                ", REFERENCE3='" + REFERENCE3 + '\'' +
                ", ASSIGNMENT='" + ASSIGNMENT + '\'' +
                ", BR_LINE=" + BR_LINE +
                ", PAYMENT_CENTER='" + PAYMENT_CENTER + '\'' +
                ", TRADING_PARTNER='" + TRADING_PARTNER + '\'' +
                ", COMP_CODE='" + COMP_CODE + '\'' +
                ", PO_LINE=" + PO_LINE +
                ", MR_LINE='" + MR_LINE + '\'' +
                ", INCOME='" + INCOME + '\'' +
                ", AUTOGEN=" + AUTOGEN +
                ", IS_WTX=" + IS_WTX +
                ", LINE=" + LINE +
                '}';
    }
}
