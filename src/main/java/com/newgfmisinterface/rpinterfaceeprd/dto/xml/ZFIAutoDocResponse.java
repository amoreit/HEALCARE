package com.newgfmisinterface.rpinterfaceeprd.dto.xml;


import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.newgfmisinterface.rpinterfaceeprd.adapter.DateTimeAdapter;
import com.newgfmisinterface.rpinterfaceeprd.adapter.TimestampAdapter;

@XmlRootElement(
        name = "RESPONSE"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class ZFIAutoDocResponse extends BaseMessage {
    @XmlElement(
            name = "DOC_TYPE"
    )
    private String docType = null;
    @XmlElement(
            name = "COMP_CODE"
    )
    private String compCode = null;
    @XmlElement(
            name = "ACC_DOC_NO"
    )
    private String accDocNo = null;
    @XmlElement(
            name = "FISCAL_YEAR"
    )
    private String fiscalYear = null;
    @XmlElement(
            name = "DOC_STATUS"
    )
    private String docStatus = null;
    @XmlElement(
            name = "AMOUNT"
    )
    private BigDecimal amount = null;
    @XmlElement(
            name = "PAYMENT_BLOCK"
    )
    private String paymentBlock = null;
    @XmlElement(
            name = "REV_DOC_TYPE"
    )
    private String revDocType = null;
    @XmlElement(
            name = "REV_ACC_DOC_NO"
    )
    private String revAccDocNo = null;
    @XmlElement(
            name = "REV_FISCAL_YEAR"
    )
    private String revFiscalYear = null;
    @XmlElement(
            name = "REV_DATE_ACCT"
    )
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private Timestamp revDatePost = null;
    @XmlElement(
            name = "REV_REASON"
    )
    private String revReason = null;

    public ZFIAutoDocResponse() {
    }

    public String getDocType() {
        return this.docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getCompCode() {
        return this.compCode;
    }

    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getAccDocNo() {
        return this.accDocNo;
    }

    public void setAccDocNo(String accDocNo) {
        this.accDocNo = accDocNo;
    }

    public String getFiscalYear() {
        return this.fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public String getDocStatus() {
        return this.docStatus;
    }

    public void setDocStatus(String docStatus) {
        this.docStatus = docStatus;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaymentBlock() {
        return this.paymentBlock;
    }

    public void setPaymentBlock(String paymentBlock) {
        this.paymentBlock = paymentBlock;
    }

    public String getRevDocType() {
        return this.revDocType;
    }

    public void setRevDocType(String revDocType) {
        this.revDocType = revDocType;
    }

    public String getRevAccDocNo() {
        return this.revAccDocNo;
    }

    public void setRevAccDocNo(String revAccDocNo) {
        this.revAccDocNo = revAccDocNo;
    }

    public String getRevFiscalYear() {
        return this.revFiscalYear;
    }

    public void setRevFiscalYear(String revFiscalYear) {
        this.revFiscalYear = revFiscalYear;
    }

    public Timestamp getRevDatePost() {
        return this.revDatePost;
    }

    public void setRevDatePost(Timestamp revDatePost) {
        this.revDatePost = revDatePost;
    }

    public String getRevReason() {
        return this.revReason;
    }

    public void setRevReason(String revReasonPost) {
        this.revReason = revReasonPost;
    }
}

