package com.newgfmisinterface.rpinterfaceeprd.dto.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class BaseMQServiceXML {
    @XmlElement(
            name = "ID"
    )
    private String id = null;
    @XmlElement(
            name = "TYPE"
    )
    private String type = null;
    @XmlElement(
            name = "FROM"
    )
    private String from = null;
    @XmlElement(
            name = "TO"
    )
    private String to = null;

    public BaseMQServiceXML() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return this.to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
