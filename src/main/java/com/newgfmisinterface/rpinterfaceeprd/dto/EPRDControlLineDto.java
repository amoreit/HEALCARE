package com.newgfmisinterface.rpinterfaceeprd.dto;

public class EPRDControlLineDto {
    private String lineType;
    private String totalLineCount;
    private String internalUsed;

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getTotalLineCount() {
        return totalLineCount;
    }

    public void setTotalLineCount(String totalLineCount) {
        this.totalLineCount = totalLineCount;
    }

    public String getInternalUsed() {
        return internalUsed;
    }

    public void setInternalUsed(String internalUsed) {
        this.internalUsed = internalUsed;
    }
}
