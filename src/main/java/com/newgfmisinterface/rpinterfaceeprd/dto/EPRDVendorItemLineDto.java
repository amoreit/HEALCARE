package com.newgfmisinterface.rpinterfaceeprd.dto;

public class EPRDVendorItemLineDto {
    private String lineType;
    private String postingKey;
    private String accountType;
    private String vendorNo;
    private String businessArea;
    private String costCenter;
    private String fund;
    private String fundCenter;
    private String functionalArea;
    private String businessProcess;
    private String amount;
    private String depositReference;
    private String assignment;
    private String earmarkedFundNo;
    private String earmarkedFundItem;
    private String bank;
    private String gpsc;
    private String subAccount;
    private String subAccountOwner;
    private String paymentCenter;
    private String depositOwner;
    private String deposit;
    private String text;
    private String paymentTerm;
    private String paymentMethod;
    private String wtType;
    private String wtCode;
    private String wtBaseAmount;
    private String wtAmount;
    private String wtTypeFine;
    private String wtCodeFine;
    private String wtBaseAmountFine;
    private String wtAmountFine;
    private String searchTerm;
    private String vendorDeposit;
    private String bankKey;
    private String companyIDTradingPartner;
    private String tradingBA;
    private String vendorAccName;

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getPostingKey() {
        return postingKey;
    }

    public void setPostingKey(String postingKey) {
        this.postingKey = postingKey;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getVendorNo() {
        return vendorNo;
    }

    public void setVendorNo(String vendorNo) {
        this.vendorNo = vendorNo;
    }

    public String getBusinessArea() {
        return businessArea;
    }

    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getFund() {
        return fund;
    }

    public void setFund(String fund) {
        this.fund = fund;
    }

    public String getFundCenter() {
        return fundCenter;
    }

    public void setFundCenter(String fundCenter) {
        this.fundCenter = fundCenter;
    }

    public String getFunctionalArea() {
        return functionalArea;
    }

    public void setFunctionalArea(String functionalArea) {
        this.functionalArea = functionalArea;
    }

    public String getBusinessProcess() {
        return businessProcess;
    }

    public void setBusinessProcess(String businessProcess) {
        this.businessProcess = businessProcess;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDepositReference() {
        return depositReference;
    }

    public void setDepositReference(String depositReference) {
        this.depositReference = depositReference;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getEarmarkedFundNo() {
        return earmarkedFundNo;
    }

    public void setEarmarkedFundNo(String earmarkedFundNo) {
        this.earmarkedFundNo = earmarkedFundNo;
    }

    public String getEarmarkedFundItem() {
        return earmarkedFundItem;
    }

    public void setEarmarkedFundItem(String earmarkedFundItem) {
        this.earmarkedFundItem = earmarkedFundItem;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getGpsc() {
        return gpsc;
    }

    public void setGpsc(String gpsc) {
        this.gpsc = gpsc;
    }

    public String getSubAccount() {
        return subAccount;
    }

    public void setSubAccount(String subAccount) {
        this.subAccount = subAccount;
    }

    public String getSubAccountOwner() {
        return subAccountOwner;
    }

    public void setSubAccountOwner(String subAccountOwner) {
        this.subAccountOwner = subAccountOwner;
    }

    public String getPaymentCenter() {
        return paymentCenter;
    }

    public void setPaymentCenter(String paymentCenter) {
        this.paymentCenter = paymentCenter;
    }

    public String getDepositOwner() {
        return depositOwner;
    }

    public void setDepositOwner(String depositOwner) {
        this.depositOwner = depositOwner;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getWtType() {
        return wtType;
    }

    public void setWtType(String wtType) {
        this.wtType = wtType;
    }

    public String getWtCode() {
        return wtCode;
    }

    public void setWtCode(String wtCode) {
        this.wtCode = wtCode;
    }

    public String getWtBaseAmount() {
        return wtBaseAmount;
    }

    public void setWtBaseAmount(String wtBaseAmount) {
        this.wtBaseAmount = wtBaseAmount;
    }

    public String getWtAmount() {
        return wtAmount;
    }

    public void setWtAmount(String wtAmount) {
        this.wtAmount = wtAmount;
    }

    public String getWtTypeFine() {
        return wtTypeFine;
    }

    public void setWtTypeFine(String wtTypeFine) {
        this.wtTypeFine = wtTypeFine;
    }

    public String getWtCodeFine() {
        return wtCodeFine;
    }

    public void setWtCodeFine(String wtCodeFine) {
        this.wtCodeFine = wtCodeFine;
    }

    public String getWtBaseAmountFine() {
        return wtBaseAmountFine;
    }

    public void setWtBaseAmountFine(String wtBaseAmountFine) {
        this.wtBaseAmountFine = wtBaseAmountFine;
    }

    public String getWtAmountFine() {
        return wtAmountFine;
    }

    public void setWtAmountFine(String wtAmountFine) {
        this.wtAmountFine = wtAmountFine;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public String getVendorDeposit() {
        return vendorDeposit;
    }

    public void setVendorDeposit(String vendorDeposit) {
        this.vendorDeposit = vendorDeposit;
    }

    public String getBankKey() {
        return bankKey;
    }

    public void setBankKey(String bankKey) {
        this.bankKey = bankKey;
    }

    public String getCompanyIDTradingPartner() {
        return companyIDTradingPartner;
    }

    public void setCompanyIDTradingPartner(String companyIDTradingPartner) {
        this.companyIDTradingPartner = companyIDTradingPartner;
    }

    public String getTradingBA() {
        return tradingBA;
    }

    public void setTradingBA(String tradingBA) {
        this.tradingBA = tradingBA;
    }

    public String getVendorAccName() {
        return vendorAccName;
    }

    public void setVendorAccName(String vendorAccName) {
        this.vendorAccName = vendorAccName;
    }
}
