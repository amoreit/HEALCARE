package com.newgfmisinterface.rpinterfaceeprd.dto;

public class EPRDHeaderLineDto {
    private String lineType;
    private String documentType;
    private String companyCode;
    private String documentDate;
    private String postingDate;
    private String reference;
    private String currency;
    private String reverseDate;
    private String reversalReason;
    private String PaymentCenterHeader;

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getReverseDate() {
        return reverseDate;
    }

    public void setReverseDate(String reverseDate) {
        this.reverseDate = reverseDate;
    }

    public String getReversalReason() {
        return reversalReason;
    }

    public void setReversalReason(String reversalReason) {
        this.reversalReason = reversalReason;
    }

    public String getPaymentCenterHeader() {
        return PaymentCenterHeader;
    }

    public void setPaymentCenterHeader(String paymentCenterHeader) {
        this.PaymentCenterHeader = paymentCenterHeader;
    }
}
