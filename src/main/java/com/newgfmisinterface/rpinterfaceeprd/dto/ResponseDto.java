package com.newgfmisinterface.rpinterfaceeprd.dto;

public class ResponseDto {

    //returned code (0=fail, 1=success)
    private int code;
    //returned message
    private String message;
    //returned data
    private Object data;

    public ResponseDto() {

    }

    public ResponseDto(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
