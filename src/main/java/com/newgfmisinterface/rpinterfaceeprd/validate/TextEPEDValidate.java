package com.newgfmisinterface.rpinterfaceeprd.validate;

import com.newgfmisinterface.rpinterfaceeprd.dto.EPRDControlLineDto;
import com.newgfmisinterface.rpinterfaceeprd.dto.EPRDHeaderLineDto;
import com.newgfmisinterface.rpinterfaceeprd.dto.EPRDAccountItemLineDto;

public class TextEPEDValidate {
    public TextEPEDValidate() {
    }

    public EPRDHeaderLineDto substringHeaderLineObj(String textLine) {

        EPRDHeaderLineDto headerLineDto = new EPRDHeaderLineDto();

        String lineType = textLine.substring(0, 1);
        String trimLineType = lineType.trim();
        headerLineDto.setLineType(trimLineType);

        String documentType = textLine.substring(1, 3);
        String trimDocumentType = documentType.trim();
        headerLineDto.setDocumentType(trimDocumentType);

        String companyCode = textLine.substring(3, 7);
        String trimCompanyCode = companyCode.trim();
        headerLineDto.setCompanyCode(trimCompanyCode);

        String documentDate = textLine.substring(7, 15);
        String trimDocumentDate = documentDate.trim();
        headerLineDto.setDocumentDate(trimDocumentDate);

        String postingDate = textLine.substring(15, 23);
        String trimPostingDate = postingDate.trim();
        headerLineDto.setPostingDate(trimPostingDate);

        String reference = textLine.substring(23, 39);
        String trimReference = reference.trim();
        headerLineDto.setReference(trimReference);

        String currency = textLine.substring(39, 44);
        String trimCurrency = currency.trim();
        headerLineDto.setCurrency(trimCurrency);

        String dateListReversal = textLine.substring(44, 52);
        String trimDateListReversal = dateListReversal.trim();
        headerLineDto.setReverseDate(trimDateListReversal);

        String reversalReason = textLine.substring(52, 54);
        String trimReversalReason = reversalReason.trim();
        headerLineDto.setReversalReason(trimReversalReason);

        String disbursementUnit = textLine.substring(54, 69);
        String trimDisbursementUnit = disbursementUnit.trim();
        headerLineDto.setPaymentCenterHeader(trimDisbursementUnit);

        return headerLineDto;
    }

    public EPRDAccountItemLineDto substringItemLineObj(String textLine) {

        EPRDAccountItemLineDto itemLineDto = new EPRDAccountItemLineDto();

        String lineType = textLine.substring(0, 1);
        String trimLineType = lineType.trim();
        itemLineDto.setLineType(trimLineType);

        String postingKey = textLine.substring(1, 3);
        String trimPostingKey = postingKey.trim();
        itemLineDto.setPostingKey(trimPostingKey);

        String accountType = textLine.substring(3, 4);
        String trimAccountType = accountType.trim();
        itemLineDto.setAccountType(trimAccountType);

        String accountCode = textLine.substring(4, 14);
        String trimAccountCode = accountCode.trim();
        itemLineDto.setAccountCode(trimAccountCode);

        String businessArea = textLine.substring(14, 18);
        String trimBusinessArea = businessArea.trim();
        itemLineDto.setBusinessArea(trimBusinessArea);

        String costCenter = textLine.substring(18, 28);
        String trimCostCenter = costCenter.trim();
        itemLineDto.setCostCenter(trimCostCenter);

        String fund = textLine.substring(28, 38);
        String trimFund = fund.trim();
        itemLineDto.setFund(trimFund);

        String fundCenter = textLine.substring(38, 54);
        String trimFundCenter = fundCenter.trim();
        itemLineDto.setFundCenter(trimFundCenter);

        String functionalArea = textLine.substring(54, 70);
        String trimFunctionalArea = functionalArea.trim();
        itemLineDto.setFunctionalArea(trimFunctionalArea);

        String businessProcess = textLine.substring(70, 82);
        String trimBusinessProcess = businessProcess.trim();
        itemLineDto.setBusinessProcess(trimBusinessProcess);

        String amount = textLine.substring(82, 98);
        String trimAmount = amount.trim();
        itemLineDto.setAmount(trimAmount);

        String depositReference = textLine.substring(98, 118);
        String trimDepositReference = depositReference.trim();
        itemLineDto.setDepositReference(trimDepositReference);

        String assignment = textLine.substring(118, 136);
        String trimAssignment = assignment.trim();
        itemLineDto.setAssignment(trimAssignment);

        String earmarkedFundNo = textLine.substring(137, 146);
        String trimEarmarkedFundNo = earmarkedFundNo.trim();
        itemLineDto.setEarmarkedFundNo(trimEarmarkedFundNo);

        String earmarkedFundItem = textLine.substring(146, 149);
        String trimEarmarkedFundItem = earmarkedFundItem.trim();
        itemLineDto.setEarmarkedFundItem(trimEarmarkedFundItem);

        String bank = textLine.substring(149, 155);
        String trimBank = bank.trim();
        itemLineDto.setBank(trimBank);

        String gpsc = textLine.substring(155, 169);
        String trimGpsc = gpsc.trim();
        itemLineDto.setGpsc(trimGpsc);

        String subAccount = textLine.substring(169, 176);
        String trimSubAccount = subAccount.trim();
        itemLineDto.setSubAccount(trimSubAccount);

        String subAccountOwner = textLine.substring(176, 186);
        String trimSubAccountOwner = subAccountOwner.trim();
        itemLineDto.setSubAccountOwner(trimSubAccountOwner);

        String paymentCenter = textLine.substring(186, 201);
        String trimPaymentCenter = paymentCenter.trim();
        itemLineDto.setPaymentCenter(trimPaymentCenter);

        String depositOwner = textLine.substring(201, 211);
        String trimDepositOwner = depositOwner.trim();
        itemLineDto.setDepositOwner(trimDepositOwner);

        String deposit = textLine.substring(211, 216);
        String trimDeposit = deposit.trim();
        itemLineDto.setDeposit(trimDeposit);

        String text = textLine.substring(216, 266);
        String trimText = text.trim();
        itemLineDto.setText(trimText);

        String paymentTerm = textLine.substring(266, 270);
        String trimPaymentTerm = paymentTerm.trim();
        itemLineDto.setPaymentTerm(trimPaymentTerm);

        String paymentMethod = textLine.substring(270, 271);
        String trimPaymentMethod = paymentMethod.trim();
        itemLineDto.setPaymentMethod(trimPaymentMethod);

        String wtType = textLine.substring(271, 273);
        String trimWtType = wtType.trim();
        itemLineDto.setWtType(trimWtType);

        String wtCode = textLine.substring(273, 275);
        String trimWtCode = wtCode.trim();
        itemLineDto.setWtCode(trimWtCode);

        String wtBaseAmount = textLine.substring(275, 291);
        String trimWtBaseAmount = wtBaseAmount.trim();
        itemLineDto.setWtBaseAmount(trimWtBaseAmount);

        String wtAmount = textLine.substring(291, 307);
        String trimWtAmount = wtAmount.trim();
        itemLineDto.setWtAmount(trimWtAmount);

        String wtTypeFine = textLine.substring(307, 309);
        String trimWtTypeFine = wtTypeFine.trim();
        itemLineDto.setWtTypeFine(trimWtTypeFine);

        String wtCodeFine = textLine.substring(309, 311);
        String trimWtCodeFine = wtCodeFine.trim();
        itemLineDto.setWtCodeFine(trimWtCodeFine);

        String wtBaseAmountFine = textLine.substring(311, 327);
        String trimWtBaseAmountFine = wtBaseAmountFine.trim();
        itemLineDto.setWtBaseAmountFine(trimWtBaseAmountFine);

        String wtAmountFine = textLine.substring(327, 343);
        String trimWtAmountFine = wtAmountFine.trim();
        itemLineDto.setWtAmountFine(trimWtAmountFine);

        String searchTerm = textLine.substring(343, 363);
        String trimSearchTerm = searchTerm.trim();
        itemLineDto.setSearchTerm(trimSearchTerm);

        String vendorDeposit = textLine.substring(363, 381);
        String trimVendorDeposit = vendorDeposit.trim();
        itemLineDto.setVendorDeposit(trimVendorDeposit);

        String bankKey = textLine.substring(381, 396);
        String trimBankKey = bankKey.trim();
        itemLineDto.setBankKey(trimBankKey);

        String companyIDTradingPartner = textLine.substring(396, 402);
        String trimCompanyIDTradingPartner = companyIDTradingPartner.trim();
        itemLineDto.setCompanyIDTradingPartner(trimCompanyIDTradingPartner);

        String tradingBA = textLine.substring(403, 406);
        String trimTradingBA = tradingBA.trim();
        itemLineDto.setTradingBA(trimTradingBA);

        String vendorAccName = textLine.substring(407, 446);
        String trimVendorAccName = vendorAccName.trim();
        itemLineDto.setVendorAccName(trimVendorAccName);

        return itemLineDto;
    }

    public EPRDControlLineDto substringControlLineObj(String textLine) {

        EPRDControlLineDto controlLineDto = new EPRDControlLineDto();

        String lineType = textLine.substring(0, 1);
        String trimLineType = lineType.trim();
        controlLineDto.setLineType(trimLineType);

        String totalLineCount = textLine.substring(1, 7);
        String trimTotalLineCount = totalLineCount.trim();
        controlLineDto.setTotalLineCount(trimTotalLineCount);

        String internalUsed = textLine.substring(7, 87);
        String trimInternalUsed = internalUsed.trim();
        controlLineDto.setInternalUsed(trimInternalUsed);

        return controlLineDto;
    }
}
