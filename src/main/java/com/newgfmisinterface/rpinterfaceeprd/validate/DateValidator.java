package com.newgfmisinterface.rpinterfaceeprd.validate;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateValidator {
	
	private static Logger LOGGER = LoggerFactory.getLogger(DateValidator.class);
	
	public boolean isThisDateValid(String dateToValidate, String dateFromat){
		
		if(dateToValidate == null){
			return false;
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		
		try {
			
			//if not valid, it will throw ParseException
			sdf.parse(dateToValidate);
			 
		
		} catch (ParseException e) {
			
			e.printStackTrace();
			LOGGER.error("ParseException ERROR", e);
			return false;
		}
		
		return true;
	}

}
