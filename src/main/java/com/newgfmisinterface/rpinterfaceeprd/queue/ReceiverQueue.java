//package com.newgfmisinterface.eprd.queue;
//
//import com.newgfmisinterface.eprd.dao.EbiddingActiveMQDao;
//import com.newgfmisinterface.eprd.dto.MqMsgReplyDto;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jms.annotation.JmsListener;
//import org.springframework.stereotype.Component;
//
////import com.newgfmisinterface.rpinterfaceebidding.dto.MqMsgDto;
//import com.newgfmisinterface.eprd.utils.AES;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Unmarshaller;
//import javax.xml.transform.stream.StreamSource;
//import java.io.StringReader;
//
//
//@Component
//public class ReceiverQueue {
//    private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverQueue.class);
//
//    @Autowired
//    private EbiddingActiveMQDao ebiddingActiveMQDao;
//
//    //    @JmsListener(destination = "${spring.activemq.queue.consumer}", containerFactory = "jsaFactory")
//    @JmsListener(destination = "${spring.activemq.queue.consumer}", containerFactory="jsaFactory")
//    public void appleReceive(String message) {
//        final String secretKey = "123456789";
//
//        String decryptedString = AES.decrypt(message, secretKey);
//
//        LOGGER.info("received message not encrypte='{}'", decryptedString);
//
//        MqMsgReplyDto mqMsgObj = jaxbXmlMsgToObject(decryptedString);
//
//        LOGGER.info("received message mqMsgObj ='{}'", mqMsgObj.toString());
//
//        try {
//            ebiddingActiveMQDao.updateLogMQCode(mqMsgObj);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    private static MqMsgReplyDto jaxbXmlMsgToObject(String xmlmessage) {
//
//        JAXBContext jaxbContext;
//        MqMsgReplyDto mqMsgReplyDto;
//        try {
//            StreamSource streamSource = new StreamSource(new StringReader(xmlmessage));
//
//            jaxbContext = JAXBContext.newInstance(MqMsgReplyDto.class);
//            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//
//            mqMsgReplyDto = (MqMsgReplyDto) jaxbUnmarshaller.unmarshal(streamSource);
//
//            return mqMsgReplyDto;
//
//        } catch (JAXBException e) {
//            mqMsgReplyDto = new MqMsgReplyDto();
//            e.printStackTrace();
//            return null;
//        }
//    }
//}
//
