//package com.newgfmisinterface.eprd.queue;
//
//import com.newgfmisinterface.eprd.utils.AES;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.jms.core.JmsTemplate;
//import org.springframework.stereotype.Component;
//
//@Component
//public class SenderQueue {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(SenderQueue.class);
//
//    @Autowired
//    JmsTemplate jmsTemplate;
//
//    private AES aes = new AES();
//
//    @Value("${spring.activemq.queue.producer}")
//    private String queueProducer;
//
//    public void send(String message){
//        String messageMQ = aes.encrypt(message, "123456789");
//        LOGGER.info("sending message='{}'", messageMQ);
////        	jmsTemplate.setTimeToLive(3000);
//        jmsTemplate.convertAndSend(queueProducer, messageMQ);
////        jmsTemplate.convertAndSend("helloworld.q", messageMQ, m -> {
////            m.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, 100000);
////            return m;
////        });
////        jmsTemplate.convertAndSend(queueProducer, messageMQ, new MessagePostProcessor() {
////            public Message postProcessMessage(Message messageMQ) throws JMSException {
////System.out.println("<<< postProcessMessage>>>>>>>>>>>>>>>>>: " + messageMQ);
//////                message.setStringProperty("company", companyName);
////                return messageMQ;
////            }
////        });
//    }
//}
