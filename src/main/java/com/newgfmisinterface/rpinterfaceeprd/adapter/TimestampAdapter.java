package com.newgfmisinterface.rpinterfaceeprd.adapter;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class TimestampAdapter extends XmlAdapter<String, Timestamp> {
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    public TimestampAdapter() {
    }

    public Timestamp unmarshal(String v) throws Exception {
        if (v == null) {
            return null;
        } else {
            Date d = this.df.parse(v);
            return new Timestamp(d.getTime());
        }
    }

    public String marshal(Timestamp v) throws Exception {
        if (v == null) {
            return null;
        } else {
            Date d = new Date(v.getTime());
            return this.df.format(d);
        }
    }
}

//public class TimeStampAdapter extends XmlAdapter<String, Date> {
//
//    /**
//     * Thread safe {@link DateFormat}.
//     */
//    private static final ThreadLocal<DateFormat> DATE_FORMAT_TL = new ThreadLocal<DateFormat>() {
//
//        @Override
//        protected DateFormat initialValue() {
//            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        }
//
//    };
//
//    @Override
//    public Date unmarshal(String v) throws Exception {
//        return DATE_FORMAT_TL.get().parse(v);
//    }
//
//    @Override
//    public String marshal(Date v) throws Exception {
//        return DATE_FORMAT_TL.get().format(v);
//    }
//
//}