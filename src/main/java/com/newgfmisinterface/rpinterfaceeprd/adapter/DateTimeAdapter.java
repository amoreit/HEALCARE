package com.newgfmisinterface.rpinterfaceeprd.adapter;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DateTimeAdapter extends XmlAdapter<String, Timestamp> {
    public static final String FORMAT_DATETIME_DISPLAY = "yyyy-MM-dd hh:mm:ss";
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public DateTimeAdapter() {
    }

    public Timestamp unmarshal(String v) throws Exception {
        if (v == null) {
            return null;
        } else {
            Date d = this.df.parse(v);
            return new Timestamp(d.getTime());
        }
    }

    public String marshal(Timestamp v) throws Exception {
        if (v == null) {
            return null;
        } else {
            LocalDateTime date = (new Date(v.getTime())).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            DateTimeFormatter patternFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
            return patternFormatter.format(date);
        }
    }
}

