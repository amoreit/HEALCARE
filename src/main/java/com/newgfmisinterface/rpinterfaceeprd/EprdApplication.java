package com.newgfmisinterface.rpinterfaceeprd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.newgfmisinterface.rpinterfaceeprd.properties.FileStorageProperties;


@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class EprdApplication {

	public static void main(String[] args) {
		SpringApplication.run(EprdApplication.class, args);
	}

}
