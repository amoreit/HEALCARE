package com.newgfmisinterface.rpinterfaceeprd.controller;

import com.newgfmisinterface.rpinterfaceeprd.dao.EprdSendPathDao;
//import com.newgfmisinterface.eprd.job.ProcessJobAMQ;
import com.newgfmisinterface.rpinterfaceeprd.dto.RequestFrontDto;
import com.newgfmisinterface.rpinterfaceeprd.dto.ResponseDto;
import com.newgfmisinterface.rpinterfaceeprd.job.ProcessJobEPED;
import com.newgfmisinterface.rpinterfaceeprd.service.FileStorageService;
import com.newgfmisinterface.rpinterfaceeprd.service.ScanFilePathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class ExecuteTaskController {
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ScanFilePathService scanFilePath;

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private EprdSendPathDao eprdSendPathDao;

//    @Autowired
//    private ProcessJobAMQ processJobAMQ;

    @Autowired
    private ProcessJobEPED processJobEPED;

//    @Autowired
//    private EbiddingLogDao ebiddingLogDao;
//
//    @Autowired
//    private EbiddingItemDao ebiddingItemDao;
//
//    @Autowired
//    private EbiddingStatementDao ebiddingStatementDao;
//
//    @Autowired
//    private EbiddingErrorDao ebiddingErrorDao;
//
//    @Autowired
//    private EbiddingActiveMQDao ebiddingActiveMQDao;

    @GetMapping("/testConvert")
    public ResponseEntity<String> executeTaskEBD() {
        long startTimeTotal = System.currentTimeMillis();

        String[] projectFile = {"05889225A004EXCINFCT01-LINFCDDRUG2","408666272102EXCINFCT01-LINFCDDRUG2"};
        List<String> projectName = new ArrayList<>();
        int lengthProject = projectFile.length;
        for (String projectCode : projectFile) {

            long startTime = System.currentTimeMillis();
            List<String> moveFile = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectCode);

            int fileListForScan = 0;
            for (String moveFileToProcess : moveFile) {
                File fileName = new File(moveFileToProcess);
                String getFileName = fileName.getName();
                projectName.add(getFileName);
                fileStorageService.moveFileToProcessDir(moveFileToProcess);
                fileListForScan++;
            }
            if (fileListForScan > 0) {
                List<String> fileList = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);
                for (String filename : fileList) {
                    processJobEPED.jobRunner(filename);
                }
            }
            lengthProject--;

            long endTime = System.currentTimeMillis();
            long duration = (endTime - startTime);
            System.out.print("Execution time is " + duration + " milli seconds\n");

//            if (lengthProject == 0) {
//                for (String projectname : projectName) {
//                    processJobAMQ.jobRunner(projectname, 1);
//                    try {
//                        int checkLoop = ebiddingActiveMQDao.countItemFileError(projectname);
//                        if (checkLoop > 0) {
//                            for (int i = 1; i <= 2; i++) {
//                                processJobAMQ.jobRunner(projectname, 2);
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            }
        }

        long endTimeTotal = System.currentTimeMillis();
        long durationTotal = (endTimeTotal - startTimeTotal);
        System.out.print("Completed time is " + durationTotal + " milli seconds");

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        return new ResponseEntity<String>("<h1>Job Task e-bidding is Running at Time : " + timeStamp + "</h1>", HttpStatus.OK);
    }
    @GetMapping("/convertFileEprd")
    public ResponseDto convertFileEbidding(RequestFrontDto requestFrontDto) {
        System.out.println(requestFrontDto);
//        ArrayList<List<Map<String, Object>>> resultList = null;
        String projectFile = requestFrontDto.getFileName();
//        String projectFile = "EE202003140933130011002EXCINFCT01.TXT";
        List<String> moveFile = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectFile);

        String moveFileName = moveFile.toString();
        moveFileName = moveFileName.replaceAll("[\\[\\]]", "");
        if (moveFileName.equals(null) || moveFileName.isEmpty()) {
            return new ResponseDto(2, "fail", null);
        } else {
            boolean statusMove = fileStorageService.moveFileToConvert(moveFileName);
            if (statusMove) {
                List<String> fileList = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectFile);
//                String transDate = requestFrontDto.getTransDate();
                String fileName = fileList.toString();
                fileName = fileName.replaceAll("[\\[\\]]", "");
                try {
                    processJobEPED.jobRunner(fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try {
//            if(resultList.size() > 0){
            return new ResponseDto(1, "success", null);
//            }else {
//                return new ResponseDto(0, "fail", null);
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", null);
        }


    }
    @GetMapping("/testConvertFileEprd")
    public ResponseDto testConvertFileEprd(RequestFrontDto requestFrontDto) {
        System.out.println(requestFrontDto);
//        ArrayList<List<Map<String, Object>>> resultList = null;
        String projectFile = requestFrontDto.getFileName();
//        String projectFile = "EE202003140933130011002EXCINFCT01.TXT";
        List<String> moveFile = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectFile);

        String moveFileName = moveFile.toString();
        moveFileName = moveFileName.replaceAll("[\\[\\]]", "");
        if (moveFileName.equals(null) || moveFileName.isEmpty()) {
            return new ResponseDto(2, "fail", null);
        } else {
            boolean statusMove = fileStorageService.moveFileToConvert(moveFileName);
            if (statusMove) {
                List<String> fileList = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectFile);
//                String transDate = requestFrontDto.getTransDate();
                String fileName = fileList.toString();
                fileName = fileName.replaceAll("[\\[\\]]", "");
                try {
                    processJobEPED.jobRunner(fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try {
//            if(resultList.size() > 0){
            return new ResponseDto(1, "success", null);
//            }else {
//                return new ResponseDto(0, "fail", null);
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", null);
        }


    }

    @GetMapping("/eprdPathUploadFile")
    public ResponseDto getEprdPathUploadFile() {

        List<Map<String, Object>> resultList = null;
        try {
            resultList = eprdSendPathDao.getEprdPathUploadFile();
            System.out.println("<<< resultList: " + resultList);
            return new ResponseDto(1, "success", resultList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", resultList);
        }
    }
    @GetMapping("/eprdPathOutputFile")
    public ResponseDto getEprdPathOutputFile() {

        List<Map<String, Object>> resultList = null;
        try {
            resultList = eprdSendPathDao.getEprdPathOutputFile();
            System.out.println("<<< resultList: " + resultList);
            return new ResponseDto(1, "success", resultList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", resultList);
        }
    }
}
