package com.newgfmisinterface.rpinterfaceeprd.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class EprdSendPathDao {
    private static Logger LOGGER = LoggerFactory.getLogger(EprdSendPathDao.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Map<String, Object>> getEprdPathUploadFile() throws Exception {
        List<Map<String, Object>> resultList = null;
        String sql ="SELECT VALUE AS uploadpath FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='INPUT_PATH_EPRD_BATCHJOB'";
        System.out.println("<<< sql: " + sql);
        resultList = jdbcTemplate.queryForList(sql);
        return resultList;
    }
    public List<Map<String, Object>> getEprdPathOutputFile() throws Exception {
        List<Map<String, Object>> resultList = null;
        String sql ="SELECT VALUE AS outputpath FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='OUTPUT_PATH_EPRD_BATCHJOB'";
        System.out.println("<<< sql: " + sql);
        resultList = jdbcTemplate.queryForList(sql);
        return resultList;
    }
}
