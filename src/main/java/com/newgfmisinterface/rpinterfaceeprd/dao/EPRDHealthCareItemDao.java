package com.newgfmisinterface.rpinterfaceeprd.dao;

import com.newgfmisinterface.rpinterfaceeprd.dto.EPRDHealthCareItemDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Repository
public class EPRDHealthCareItemDao {
    private static Logger LOGGER = LoggerFactory.getLogger(EPRDHealthCareItemDao.class);
    private Executor executor = Executors.newFixedThreadPool(10);

    @Autowired
    private JdbcTemplate jdbcTemplate;
    public int addItems(EPRDHealthCareItemDto itemDto) throws Exception {
        String docDate = null;
        docDate = itemDto.getDocDate().equals("")? null: "to_timestamp('"+itemDto.getDocDate()+"','YYYYMMDD HH24:MI:SS.FF1')";
        String postingDate = null;
        postingDate = itemDto.getPostingDate().equals("")? null: "to_timestamp('"+itemDto.getPostingDate()+"','YYYYMMDD HH24:MI:SS.FF1')";
        String revDate = null;
        revDate = itemDto.getRevDate().equals("")? null: "to_timestamp('"+itemDto.getRevDate()+ "','YYYYMMDD HH24:MI:SS.FF1')";
        String vendorNo = itemDto.getPostingKey().equals("31")  && itemDto.getAccountType().equals("K") ?itemDto.getAccountCode():"";
        String accountCode = itemDto.getPostingKey().equals("40") && itemDto.getAccountType().equals("S") ? itemDto.getAccountCode() :"";

        String sql = "INSERT INTO TH_RP_TRN_HEALTHCARE_ITEM (FILE_NAME,TRANS_DATE,LINE_TYPE,ITEMNO,DOC_TYPE,AGENCY_CODE,DOC_DATE,POSTING_DATE,REF_MSG,CURRENCY," +
                "REV_DATE,REVERSAL_REASON,PAYMENT_CENTER,POSTING_KEY,ACCOUNT_TYPE,ACCOUNT_CODE,VENDOR_NO,BUSINESS_AREA,COST_CENTER,FUND," +
                "FUND_CENTER,FUNCTIONAL_AREA,BUSINESS_PROCESS,AMOUNT,DEPOSIT_REFERENCE,ASSIGNMENT,EARMARKED_FUND_NO,EARMARKED_FUND_ITEM,BANK,GPSC," +
                "SUB_ACCOUNT,SUB_ACCOUNT_OWNER,DEPOSIT_OWNER,DEPOSIT,TEXT,PAYMENT_TERM,PAYMENT_METHOD,WT_TYPE,WT_CODE,WT_BASE_AMOUNT," +
                "WT_AMOUNT,WT_TYPE_FINE,WT_CODE_FINE,WT_BASE_AMOUNT_FINE,WT_AMOUNT_FINE,SEARCH_TERM_VENDOR,VENDOR_DEPOSIT_AC,BANK_KEY,COMPANY_ID_OF_TRADING_PARTNER,TRADING_BA," +
                "VENDOR_ACC_NAME,CREATED_DATE,CREATED_PAGE,CREATED_USER,IPADDR,UPDATED_DATE,UPDATED_PAGE,UPDATED_USER,RECORD_STATUS,ISACTIVE) " +
                "values ('" + itemDto.getFileName() +"','" + itemDto.getTransDate() +"','" + itemDto.getLineType()+"','" + itemDto.getItemno()+"','" + itemDto.getDocType()+"','" + itemDto.getAgencyCode()+"'," + docDate+"," + postingDate+",'" + itemDto.getRefMsg()+"','" + itemDto.getCurrency() +"'," +
                "" + revDate +",'" + itemDto.getReversalReason() +"','" + itemDto.getPaymentCenter() +"','" + itemDto.getPostingKey() +"','" + itemDto.getAccountType() +"','" + accountCode+"','" + vendorNo +"','" + itemDto.getBusinessArea()+"','" + itemDto.getCostCenter() +"','" + itemDto.getFund() +"'," +
                "'" + itemDto.getFundCenter()+"','" + itemDto.getFunctionalArea() +"','" + itemDto.getBusinessProcess() +"','" + itemDto.getAmount() +"','" + itemDto.getDepositReference() +"','" + itemDto.getAssignment() +"','" + itemDto.getEarmarkedFundNo() +"','" + itemDto.getEarmarkedFundItem() +"','" + itemDto.getBank() +"','" + itemDto.getGpsc() +"'," +
                "'" + itemDto.getSubAccount()+"','" + itemDto.getSubAccountOwner()+"','" + itemDto.getDepositOwner()+"','" + itemDto.getDeposit()+"','" + itemDto.getText()+"','" + itemDto.getPaymentTerm() +"','" + itemDto.getPaymentMethod() +"','" + itemDto.getWtType() +"','" + itemDto.getWtCode() +"','" + itemDto.getWtBaseAmount() +"'," +
                "'" + itemDto.getWtAmount()+"','" + itemDto.getWtTypeFine() +"','" + itemDto.getWtCodeFine() +"','" + itemDto.getWtBaseAmountFine() +"','" + itemDto.getWtAmountFine() +"','" + itemDto.getSearchTermVendor() +"','" + itemDto.getVendorDepositAc() +"','" + itemDto.getBankKey()+"','" + itemDto.getCompanyIDTradingPartner()+"','" + itemDto.getTradingBA()+"'," +
                "'" + itemDto.getVendorAccName() +"','" + itemDto.getCreatedDate() +"','" + itemDto.getCreatedPage() +"','" + itemDto.getCreatedUser() +"','" + itemDto.getIpaddr()+"','" + itemDto.getUpdatedDate()+"','" + itemDto.getUpdatedPage() +"','" + itemDto.getUpdatedUser()  +"','" + itemDto.getRecordStatus() +"','" + itemDto.getIsactive() +"')";
        System.out.println("file => " + sql);
        return jdbcTemplate.update(sql);
    }

    public CompletableFuture<int[][]> batchAdd(final Collection<EPRDHealthCareItemDto> healthCareItemList, int batchSize) {
        return CompletableFuture.supplyAsync(() -> {
            long startTime = System.currentTimeMillis();

            String sql = "INSERT INTO TH_RP_TRN_HEALTHCARE_ITEM (FILE_NAME,TRANS_DATE,LINE_TYPE,ITEMNO,DOC_TYPE,AGENCY_CODE,DOC_DATE,POSTING_DATE,REF_MSG,CURRENCY," +
                    "REV_DATE,REVERSAL_REASON,PAYMENT_CENTER,POSTING_KEY,ACCOUNT_TYPE,ACCOUNT_CODE,VENDOR_NO,BUSINESS_AREA,COST_CENTER,FUND," +
                    "FUND_CENTER,FUNCTIONAL_AREA,BUSINESS_PROCESS,AMOUNT,DEPOSIT_REFERENCE,ASSIGNMENT,EARMARKED_FUND_NO,EARMARKED_FUND_ITEM,BANK,GPSC," +
                    "SUB_ACCOUNT,SUB_ACCOUNT_OWNER,DEPOSIT_OWNER,DEPOSIT,TEXT,PAYMENT_TERM,PAYMENT_METHOD,WT_TYPE,WT_CODE,WT_BASE_AMOUNT," +
                    "WT_AMOUNT,WT_TYPE_FINE,WT_CODE_FINE,WT_BASE_AMOUNT_FINE,WT_AMOUNT_FINE,SEARCH_TERM_VENDOR,VENDOR_DEPOSIT_AC,BANK_KEY,COMPANY_ID_OF_TRADING_PARTNER,TRADING_BA," +
                    "VENDOR_ACC_NAME,CREATED_DATE,CREATED_PAGE,CREATED_USER,IPADDR,UPDATED_DATE,UPDATED_PAGE,UPDATED_USER,RECORD_STATUS,ISACTIVE) " +
                    "values (?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?," +
                    "?,?,?,?,?,?,?,?,?,?)";
            int[][] updateCounts = jdbcTemplate.batchUpdate(
                    sql,
                    healthCareItemList,
                    batchSize,
                    new ParameterizedPreparedStatementSetter<EPRDHealthCareItemDto>() {
                        public void setValues(PreparedStatement ps, EPRDHealthCareItemDto argument) throws SQLException {
                            ps.setString(1, argument.getFileName());
                            ps.setString(2, argument.getTransDate());
                            ps.setString(3, argument.getLineType());
                            ps.setString(4, argument.getItemno());
                            ps.setString(5, argument.getDocType());
                            ps.setString(6, argument.getAgencyCode());
                            ps.setString(7, argument.getDocDate());
                            ps.setString(8, argument.getPostingDate());
                            ps.setString(9, argument.getRefMsg());
                            ps.setString(10, argument.getCurrency());
                            ps.setString(11, argument.getRevDate());
                            ps.setString(12, argument.getReversalReason());
                            ps.setString(13, argument.getPaymentCenter());
                            ps.setString(14, argument.getPostingKey());
                            ps.setString(15, argument.getAccountType());
                            ps.setString(16, argument.getAccountCode());
                            ps.setString(17, argument.getVendorNo());
                            ps.setString(18, argument.getBusinessArea());
                            ps.setString(19, argument.getCostCenter());
                            ps.setString(20, argument.getFund());
                            ps.setString(21, argument.getFundCenter());
                            ps.setString(22, argument.getFunctionalArea());
                            ps.setString(23, argument.getBusinessProcess());
                            ps.setString(24, argument.getAmount());
                            ps.setString(25, argument.getDepositReference());
                            ps.setString(26, argument.getAssignment());
                            ps.setString(27, argument.getEarmarkedFundNo());
                            ps.setString(28, argument.getEarmarkedFundItem());
                            ps.setString(29, argument.getBank());
                            ps.setString(30, argument.getGpsc());
                            ps.setString(31, argument.getSubAccount());
                            ps.setString(32, argument.getSubAccountOwner());
                            ps.setString(33, argument.getDepositOwner());
                            ps.setString(34, argument.getDeposit());
                            ps.setString(35, argument.getText());
                            ps.setString(36, argument.getPaymentTerm());
                            ps.setString(37, argument.getPaymentMethod());
                            ps.setString(38, argument.getWtType());
                            ps.setString(39, argument.getWtCode());
                            ps.setString(40, argument.getWtBaseAmount());
                            ps.setString(41, argument.getWtAmount());
                            ps.setString(42, argument.getWtTypeFine());
                            ps.setString(43, argument.getWtCodeFine());
                            ps.setString(44, argument.getWtBaseAmountFine());
                            ps.setString(45, argument.getWtAmountFine());
                            ps.setString(46, argument.getSearchTermVendor());
                            ps.setString(47, argument.getVendorDepositAc());
                            ps.setString(48, argument.getBankKey());
                            ps.setString(49, argument.getCompanyIDTradingPartner());
                            ps.setString(50, argument.getTradingBA());
                            ps.setString(51, argument.getVendorAccName());
                            ps.setString(52, argument.getCreatedDate());
                            ps.setString(53, argument.getCreatedPage());
                            ps.setString(54, argument.getCreatedUser());
                            ps.setString(55, argument.getIpaddr());
                            ps.setString(56, argument.getUpdatedDate());
                            ps.setString(57, argument.getUpdatedPage());
                            ps.setString(58, argument.getUpdatedUser());
                            ps.setString(59, argument.getRecordStatus());
                            ps.setString(60, argument.getIsactive());
                        }
                    });

            long endTime = System.currentTimeMillis();
            long duration = (endTime - startTime);

			System.out.println("Execution Insert  batchAdd  time is " + duration + " milli seconds - Batch Size "+ batchSize+ " Item");

            return updateCounts;

        }, executor).exceptionally(ex -> {
            LOGGER.error("Something went wrong : ", ex);
            return null;
        });
    }
}
