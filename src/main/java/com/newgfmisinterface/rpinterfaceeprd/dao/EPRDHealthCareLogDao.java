package com.newgfmisinterface.rpinterfaceeprd.dao;

import com.newgfmisinterface.rpinterfaceeprd.dto.EPRDHealthCareLogDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class EPRDHealthCareLogDao {
    private static Logger LOGGER = LoggerFactory.getLogger(EPRDHealthCareLogDao.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;
    public int addLog(EPRDHealthCareLogDto dto) throws Exception {
        String sql ="INSERT INTO TH_RP_TRN_HEALTHCARE_LOG (FILE_NAME,TRANS_DATE,FILE_SET,FILE_TYPE,STATUS,UPLOAD_START,UPLOAD_FINISH,UPLOAD_NAME,POST_START,POST_FINISH," +
                "POST_NAME,CREATED_DATE,CREATED_PAGE,CREATED_USER,IPADDR,UPDATED_DATE,UPDATED_PAGE,UPDATED_USER,RECORD_STATUS,ISACTIVE)" +
                "values ('"+dto.getFileName()+"', '"+dto.getTransDate()+"', '\"+dto.FILE_SET+\"', '\"+dto.FILE_TYPE+\"', '\"+dto.STATUS+\"', '\"+dto.UPLOAD_START+\"', '\"+dto.UPLOAD_FINISH+\"', '\"+dto.UPLOAD_NAME+\"', '\"+dto.POST_START+\"', '\"+dto.POST_FINISH+\"', '\"+dto.\n" +
                "POST_NAME+\"', '\"+dto.CREATED_DATE+\"', '\"+dto.CREATED_PAGE+\"', '\"+dto.CREATED_USER+\"', '\"+dto.IPADDR+\"', '\"+dto.UPDATED_DATE+\"', '\"+dto.UPDATED_PAGE+\"', '\"+dto.UPDATED_USER+\"', '\"+dto.RECORD_STATUS+\"', '\"+dto.ISACTIVE+\"',)";
//System.out.println("file => " + sql);
        return jdbcTemplate.update(sql);
    }
}
