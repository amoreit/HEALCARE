package com.newgfmisinterface.rpinterfaceeprd.job;

import com.newgfmisinterface.rpinterfaceeprd.dao.EPRDHealthCareItemDao;
import com.newgfmisinterface.rpinterfaceeprd.dao.EPRDHealthCareLogDao;
import com.newgfmisinterface.rpinterfaceeprd.dto.EPRDControlLineDto;
import com.newgfmisinterface.rpinterfaceeprd.dto.EPRDHeaderLineDto;
import com.newgfmisinterface.rpinterfaceeprd.dto.EPRDHealthCareItemDto;
import com.newgfmisinterface.rpinterfaceeprd.dto.EPRDAccountItemLineDto;
import com.newgfmisinterface.rpinterfaceeprd.service.FileStorageService;
import com.newgfmisinterface.rpinterfaceeprd.service.ScanFilePathService;
import com.newgfmisinterface.rpinterfaceeprd.validate.TextEPEDValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class ProcessJobEPED {
    private static Logger LOGGER = LoggerFactory.getLogger(ProcessJobEPED.class);
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ScanFilePathService scanFilePath;

    @Autowired
    private EPRDHealthCareItemDao healthCareItemDao;

    @Autowired
    private EPRDHealthCareLogDao healthCareLogDao;


    private TextEPEDValidate textEPEDValidate = new TextEPEDValidate();

    public void jobRunner(String fileName) {
        String dateCreate = new SimpleDateFormat("dd-MMM-yy").format(new Date());
        File file = new File(fileStorageService.getFileStorageLocation().toString() + "/" + fileName);
        String filenameFullPath = file.getAbsolutePath();
        List<String> fileByline = fileStorageService.readFileByLine(filenameFullPath);
        EPRDHeaderLineDto lineHeader = textEPEDValidate.substringHeaderLineObj(fileByline.get(fileByline.size() - fileByline.size()));
        EPRDControlLineDto lineControl = textEPEDValidate.substringControlLineObj(fileByline.get(fileByline.size() - 1));
        int row = 0;
        int itemNo = 1;
        for (String line : fileByline) {
            row = row + 1;
            if (row == 1 || row == fileByline.size()) {
                continue;
            }
System.out.println("line"+line);
            EPRDAccountItemLineDto lineObj = textEPEDValidate.substringItemLineObj(line);
            EPRDHealthCareItemDto saveItem = new EPRDHealthCareItemDto();
            saveItem.setFileName(fileName);
            saveItem.setTransDate(dateCreate);
            saveItem.setLineType(lineObj.getLineType());
            saveItem.setItemno(""+itemNo);
            saveItem.setDocType(lineHeader.getDocumentType());
            saveItem.setAgencyCode(lineHeader.getCompanyCode());
            saveItem.setDocDate(lineHeader.getDocumentDate());
            saveItem.setPostingDate(lineHeader.getPostingDate());
            saveItem.setRefMsg(lineHeader.getReference());
            saveItem.setCurrency(lineHeader.getCurrency());
            saveItem.setRevDate(lineHeader.getReverseDate());
            saveItem.setReversalReason(lineHeader.getReversalReason());
            saveItem.setPaymentCenter(lineHeader.getPaymentCenterHeader());
            saveItem.setPostingKey(lineObj.getPostingKey());
            saveItem.setAccountType(lineObj.getAccountType());
            saveItem.setAccountCode(lineObj.getAccountCode());
//            saveItem.setVendorNo(lineObj.getV);
            saveItem.setBusinessArea(lineObj.getBusinessArea());
            saveItem.setCostCenter(lineObj.getCostCenter());
            saveItem.setFund(lineObj.getFund());
            saveItem.setFundCenter(lineObj.getFundCenter());
            saveItem.setFunctionalArea(lineObj.getFunctionalArea());
            saveItem.setBusinessProcess(lineObj.getBusinessProcess());
            saveItem.setAmount(lineObj.getAmount());
            saveItem.setDepositReference(lineObj.getDepositReference());
            saveItem.setAssignment(lineObj.getAssignment());
            saveItem.setEarmarkedFundNo(lineObj.getEarmarkedFundNo());
            saveItem.setEarmarkedFundItem(lineObj.getEarmarkedFundItem());
            saveItem.setBank(lineObj.getBank());
            saveItem.setGpsc(lineObj.getGpsc());
            saveItem.setSubAccount(lineObj.getSubAccount());
            saveItem.setSubAccountOwner(lineObj.getSubAccountOwner());
            saveItem.setDepositOwner(lineObj.getDepositOwner());
            saveItem.setDeposit(lineObj.getDeposit());
            saveItem.setText(lineObj.getText());
            saveItem.setPaymentTerm(lineObj.getPaymentTerm());
            saveItem.setPaymentMethod(lineObj.getPaymentMethod());
            saveItem.setWtType(lineObj.getWtType());
            saveItem.setWtCode(lineObj.getWtCode());
            saveItem.setWtBaseAmount(lineObj.getWtBaseAmount());
            saveItem.setWtTypeFine(lineObj.getWtTypeFine());
            saveItem.setWtAmount(lineObj.getWtAmount());
            saveItem.setWtCodeFine(lineObj.getWtCodeFine());
            saveItem.setWtBaseAmountFine(lineObj.getWtBaseAmountFine());
            saveItem.setWtAmountFine(lineObj.getWtAmountFine());
            saveItem.setSearchTermVendor(lineObj.getSearchTerm());
            saveItem.setVendorDepositAc(lineObj.getVendorDeposit());
            saveItem.setBankKey(lineObj.getBankKey());
            saveItem.setCompanyIDTradingPartner(lineObj.getCompanyIDTradingPartner());
            saveItem.setTradingBA(lineObj.getTradingBA());
            saveItem.setVendorAccName(lineObj.getVendorAccName());
            saveItem.setCreatedDate(dateCreate);
            saveItem.setCreatedPage("");
            saveItem.setCreatedUser("");
            saveItem.setIpaddr("");
            saveItem.setUpdatedDate(dateCreate);
            saveItem.setUpdatedPage("");
            saveItem.setUpdatedUser("");
            saveItem.setRecordStatus("A");
            saveItem.setIsactive("Y");
            try {
                healthCareItemDao.addItems(saveItem);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            itemNo = itemNo+1;
        }
        try {
            fileStorageService.MoveFiletoSuccessDirDate(fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
