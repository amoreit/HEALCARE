package com.newgfmisinterface.rpinterfaceeprd.utils;

public class AppUtils {
    public static boolean isEmptyOrNull(String s) {
        return (s == null || s.isEmpty());
    }

    public static String genInsertSql(String tableName, String[] columns, Object values[]) {
        String sql = "";

        try {
            sql = "INSERT INTO " + tableName + "(";
            int index = 0;
            for (String column : columns) {
                if (index > 0) {
                    sql += ",";
                }
                sql += column;
                index++;
            }
            sql += ") VALUES (";

            index = 0;
            for (Object value : values) {
                if (index > 0) {
                    sql += ",";
                }

                if (value instanceof String) {
                    if (value == null) {
                        value = "";
                    } else {
                        sql += "'" + value + "'";
                    }
                } else {
                    sql += value;
                }

                index++;
            }

            sql += ")";

        } catch (Exception e) {
            e.printStackTrace();
        }

        return sql;
    }


    public static String genUpdateSql(String tableName, String columnIdName, Object columnIdValue, String[] columns, Object values[]) {
        String sql = "";
        String value = "";

        try {
            sql = "UPDATE " + tableName + " SET ";
            for (int i = 0; i < columns.length; i++) {
                if (i > 0) {
                    sql += ",";
                }

                if (values[i] instanceof String) {
                    if (values[i] == null) {
                        value = "";
                    } else {
                        value = "'" + values[i] + "'";
                    }
                } else {
                    value = String.valueOf(values[i]);
                }

                sql += columns[i] + " = " + value;
            }

            if (columnIdValue instanceof String) {
                columnIdValue = "'" + columnIdValue + "'";
            }

            sql += " WHERE " + columnIdName + " = " + columnIdValue;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return sql;
    }

}
