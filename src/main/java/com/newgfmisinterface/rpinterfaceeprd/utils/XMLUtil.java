package com.newgfmisinterface.rpinterfaceeprd.utils;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class XMLUtil {
    public XMLUtil() {
    }

    public static <T> T xmlUnmarshall(Class<T> clazz, String xml) throws Exception {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(new Class[]{clazz});
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            StringReader reader = new StringReader(xml);
            T header = (T) jaxbUnmarshaller.unmarshal(reader);
            reader.close();
            return header;
        } catch (Throwable var6) {
            if (var6.getMessage() == null) {
                throw new Exception("Unknown XML error");
            } else {
                throw var6;
            }
        }
    }

    public static <T> String xmlMarshall(Class<T> clazz, T o) throws Exception {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(new Class[]{clazz});
            Marshaller marshaller = jaxbContext.createMarshaller();
            StringWriter writer = new StringWriter();
            marshaller.setProperty("jaxb.fragment", true);
            marshaller.setProperty("jaxb.encoding", "UTF-8");
            marshaller.marshal(o, writer);
            String result = writer.toString();
            writer.close();
            return result;
        } catch (Throwable var6) {
            if (var6.getMessage() == null) {
                throw new Exception("Unknown XML error");
            } else {
                throw var6;
            }
        }

    }
}
