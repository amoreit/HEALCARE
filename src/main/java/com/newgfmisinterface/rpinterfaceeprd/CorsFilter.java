package com.newgfmisinterface.rpinterfaceeprd;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class CorsFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    	Enumeration<String> em = request.getHeaderNames();
		
		try {
			String s = "";
			while((s = em.nextElement()) != null) {
				System.out.println("--- requestHeader: "+s);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
    	response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "*");
        response.addHeader("Access-Control-Allow-Headers", "*");
        response.addHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin, Access-Control-Allow-Credentials, Authorization");
        response.addHeader("Access-Control-Allow-Credentials", "false");
        response.addIntHeader("Access-Control-Max-Age", 3600);
        filterChain.doFilter(request, response);
    }
}
