//package com.newgfmisinterface.eprd.schedule;
//
//import com.newgfmisinterface.eprd.dao.EbiddingErrorDao;
//import com.newgfmisinterface.eprd.dao.EbiddingItemDao;
//import com.newgfmisinterface.eprd.dao.EbiddingLogDao;
//import com.newgfmisinterface.eprd.dao.EbiddingStatementDao;
//import com.newgfmisinterface.eprd.service.FileStorageService;
//import com.newgfmisinterface.eprd.service.ScanFilePathService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.TaskScheduler;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Service;
//
//import java.io.File;
//import java.util.Date;
//import java.util.List;
//
//@EnableScheduling
//@Service
//public class AnnotationScheduler {
//    private static Logger LOGGER = LoggerFactory.getLogger(AnnotationScheduler.class);
//    @Autowired
//    private FileStorageService fileStorageService;
//
//    @Autowired
//    private ScanFilePathService scanFilePath;
//
//    @Autowired
//    private TaskScheduler taskScheduler;
//
//    @Autowired
//    private EbiddingLogDao ebiddingLogDao;
//
//    @Autowired
//    private EbiddingItemDao ebiddingItemDao;
//
//    @Autowired
//    private EbiddingStatementDao ebiddingStatementDao;
//
//    @Autowired
//    private EbiddingErrorDao ebiddingErrorDao;
//    //วินาที นาที ชัวโมง
//    private static final String SCHEDULE_RUN_TIME = "0 36 15 * * ?";
//    private static final String zone = "GMT+7:00";
//    private static final String RUNNING = SCHEDULE_RUN_TIME +","+ zone;
//
//    @Scheduled(cron = RUNNING)
//    public void runCron() {
//
//    }
//}
